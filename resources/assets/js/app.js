/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import { post, get } from "./bootstrap";

require('./bootstrap');
window.toastr = require('toastr');

import ConfirmCampaign from './components/ui/ConfirmCampaign.vue'
import AdminRefill from './components/ui/AdminRefill.vue'

import vmodal from 'vue-js-modal'
import { bus } from './bus';

import vSelect from 'vue-select'

Vue.component('v-select', vSelect);
Vue.component('admin-refill', AdminRefill);

Vue.use(vmodal, { dialog: true, dynamic: true, injectModalsContainer: true });

const app = new Vue({
    el: '#app',
    data: {
      ajaxLoading: false,
    },
    mounted() {
      bus.$on('apply-campaign', (campaign_id, customers_count) => {
        this.onStartCampaign(campaign_id, customers_count);
      });
    },
    methods: {
      // -- BUY LEADS/NUMBERS --

      // method to buy number by click on number
      onPhoneClick(event, id) {
        event.preventDefault();

        post('/api/user/purchase/phone', {'customer_id': id})
          .then(res => {
            const data = res.data;
            if(data.result === 'success') {
              event.target.outerHTML = data.phone;
              document.getElementById('main_balance').innerHTML = data.balance;

              toastr['success'](data.message)
            } else {
              toastr['error'](data.message)
            }
          })
          .catch((res) => {
            toastr['error']('Не удалось выполнить запрос, попробуйте позже');
          });

      },
      // -- START CAMPAIGN --
      // method to call modal confirm on start campaign
      startConfirm(event, campaign_id, customers_count, message) {
        event.preventDefault();

        this.$modal.show(ConfirmCampaign, {
          campaign_id: campaign_id,
          customers_count : customers_count,
          message: message
        }, {
          height: 'auto'
        });
      },
      // method get call to start campaign
      onStartCampaign(campaign_id, customers_count) {
        this.ajaxLoading = true;

        post('/api/user/campaign/'+campaign_id+'/start', {'customers_count': customers_count})
          .then(res => {
            const data = res.data;

            if(data.type === 'critical') {
              toastr['error'](data.message);
            } else if(data.type === 'success') {
              this.showSimpleDialog(data.message);

              // set icon to in process
              let button = document.getElementById('list-campaigns-'+campaign_id).firstChild;
              button.className = "btn btn-warning btn-xs disabled";
              button.disabled = true;
              button.firstChild.className = "fa fa-hourglass-half";

            } else {
              this.showSimpleDialog(data.message);
            }

            this.ajaxLoading = false;
          });
      },
      showSimpleDialog(message) {
        return this.$modal.show('dialog', {
          text: message,
          buttons: [{title: 'Ok'}
          ]
        });
      },
    }
});
