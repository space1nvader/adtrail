import jQuery from "jquery";
window.toastr = require('toastr');

(function ($) {
    toastr.options.closeButton = true;
    toastr.options.progressBar = true;
    toastr.options.preventDuplicates = true;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.js-change-select').on('change', function () {
      window.location = $(this).val()
    });

    //----------------------------- Выбор активного проекта ---------------------------------//
    $('.select-project').on('change', function (e) {
        let $this = $(this),
            $changeVal = $this.val(),
            $oldVal = $this.data('val')
        if (confirm("Уверены что хотите переключиться на другой проект?")) {
            $this.data('val', $changeVal)
            $.ajax({
                url: $this.find(':selected').data('url'),
                type: "POST",
                success: function () {
                    location.reload();
                },
                error: function () {
                    alert("Ошибка отправки данных на сервер!")
                }
            });

        } else {
            $this.val($oldVal)
        }
    })

    //----------------------------- Скрытие/показ блока при клике на radio ---------------------------------//
    $('.toggle-display-block').on('change', function() {
        toggleDisplay(this)
    })

    $('.toggle-display-block').each(function (i, elem) {
        toggleDisplay(this)
    })

    function toggleDisplay(that) {
        let $block = $($(that).data('block'))
        that.checked ? $block.show(200) : $block.hide(50)
    }

    $('#campaign_message_sms_text').on('keyup', function () {

      let textarea_val = $(this).val();
      let allowed_char_count = 134;

      // if there are any russian chars, length decrease twice
      if(/^[a-zA-Z0-9-]*$/.test(textarea_val) === false) {
        allowed_char_count = 67;
      }

      if(/%LINK%/.test(textarea_val) === true) {
        allowed_char_count = allowed_char_count -16;
      }

      let char_num = $('#campaign_message_char_num'),
        chars = textarea_val.length,
        messages = Math.ceil(chars / allowed_char_count),
        remaining = messages * allowed_char_count - (chars % (messages * allowed_char_count) || messages * allowed_char_count);

      if(textarea_val === '') {
        return char_num.text('Осталось ' + allowed_char_count);
      }

      return char_num.text(remaining + ' символов осталось. Sms-сообщений всего: [' + messages + '] ');
    })


    //----------------------- Массовая покупка номеров  ----------------------------//
    $('.js-mass-select').on('change', function () {
        var selectType = this.value
        toggleBtnBuy(selectType)
    })

    function toggleBtnBuy (selectType) {
        var $btnBuy = $('.js-buy-phone-numbers button[type="submit"]'),
            $priceOneNumber = $('#price_one_num').val()

        if (selectType == "not") {
            $('input[name="selected[]"]').each(function() {
                $(this).prop('checked', false)
            })
            $('#nm-count').text(0)
            $('#nm-sum').text(0)
            $btnBuy.hide()
        } else if (selectType == "all") {
            $('input[name="selected[]"]').each(function() {
                $(this).prop('checked', true)
            })
            var $countSelected = $('#count_all_in_db').val()
            $('#nm-count').text($countSelected)
            $('#nm-sum').text($priceOneNumber * $countSelected)
            $btnBuy.show()
        } else if (selectType == "only") {
            $('input[name="selected[]"]').each(function() {
                $(this).prop('checked', true)
            })
            var $countSelected = $('input[name="selected[]"]:checked').length
            $('#nm-count').text($countSelected)
            $('#nm-sum').text($priceOneNumber * $countSelected)
            $btnBuy.show()
        }
    }

    $(document).on('submit', 'form.js-buy-phone-numbers', function (e) {
        e.preventDefault()
        var $form = $(this),
            $selectType = $('.js-mass-select option:selected').val(),
            $only = $('input[name="selected[]"]:checked').map(function(){
                return this.value;
            }).get(),
            $except = $('input[name="selected[]"]:not(:checked)').map(function(){
                return this.value;
            }).get()

        if ($('input[name="selected[]"]:checked').length < 1 && $selectType != 'all') {
            toastr.error('Не выбрано элементов!')
            return;
        }

        $.ajax({
            url: $form.attr('action'),
            type: $form.attr('method'),
            data: {'type': $selectType, 'only': $only, 'except': $except},
            dataType: 'json',
            success: function (result) {
                var toastrType = result.toastr
                if (result.message) {
                    if (toastrType) {
                        toastr[toastrType](result.message)
                    } else {
                        alert(result.message)
                    }
                }
                setTimeout(function () {
                    location.reload();
                }, 2000)
            },
            error: function () {
                alert("Ошибка данных!")
            }
        })
    })

    $(document).on('change', 'input[name="selected[]"]', function () {
        var $btnBuy = $('.js-buy-phone-numbers button[type="submit"]'),
            $priceOneNumber = $('#price_one_num').val(),
            $countSelected = $('#nm-count').text()

        if (this.checked) {
            $('#nm-count').text(parseInt($countSelected) + 1)
        } else {
            $('#nm-count').text(parseInt($countSelected) - 1)

        }

        $countSelected = $('#nm-count').text()

        if($countSelected < 1) {
            $('#nm-count').text(0)
            $('#nm-sum').text(0)
            $btnBuy.hide();
        } else {
            $('#nm-count').text($countSelected)
            $('#nm-sum').text($priceOneNumber * $countSelected)
            $btnBuy.show()
        }
    })

})(jQuery);
