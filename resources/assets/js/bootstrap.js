/* global _ Vue */

window._ = require('lodash')
window.Popper = require('popper.js').default;

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
  window.$ = window.jQuery = require('jquery');

  require('bootstrap');
} catch (e) {}

require('admin-lte')
require('icheck')

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios')

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
  window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
  console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Vue is a modern JavaScript library for building interactive web interfaces
 * using reactive data binding and reusable components. Vue's API is clean
 * and simple, leaving you to focus on building your next great project.
 */

window.Vue = require('vue')

// Use trans function in Vue (equivalent to trans() Laravel Translations helper). See htmlheader.balde.php partial.
Vue.prototype.trans = (key) => {
  return _.get(window.trans, key, key)
};

// Laravel AdminLTE vue components
Vue.component('register-form', require('./components/auth/RegisterForm.vue'))
Vue.component('login-form', require('./components/auth/LoginForm.vue'))
Vue.component('email-reset-password-form', require('./components/auth/EmailResetPasswordForm.vue'))
Vue.component('reset-password-form', require('./components/auth/ResetPasswordForm.vue'))

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key'
// });

const jsonInstance = axios.create({
  headers: {
    "Content-Type": "application/json"
  }
});


/**
 * Вернем токен из meta тега
 *
 */
function getCSFR() {
  let api_key;
  let metas = document.getElementsByTagName("meta");
  for (var x = 0, y = metas.length; x < y; x++) {
    if (metas[x].name.toLowerCase() == "api-token") {
      api_key = metas[x];
      return api_key.content;
    }
  }
}

/**
 * Registration & session functions
 */

export const setTokenApi = access_token => {
  jsonInstance.defaultConfig.headers[
    "Authorization"
    ] = `Bearer ${access_token}`;
};

/**
 * Удалим токен из запросов
 *
 */
export const clearTokenApi = () => {
  jsonInstance.defaultConfig.headers["Authorization"] = undefined;
};

/**
 * Установим токен для запросов
 *
 */
setTokenApi(getCSFR());

/**
 * POST запрос
 * получение информации
 *
 * @param {str} url  - адрес запроса
 * @param {object} post - передаваемые данные
 */
export const post = (url, post) =>
  jsonInstance
    .post(url, post)
    .then(response => {
      const { status, message, data } = response;
      return { status, message, data };
    })
    .catch(error => {
      const { status, message, data } = error.response.data;
      return Promise.reject({ status, message, data });
    });

export const get = (url, get) =>
  jsonInstance
    .get(url, get)
    .then(response => {
      const { status, message, data } = response;
      return { status, message, data };
    })
    .catch(error => {
      const { status, message, data } = error.response.data;
      return Promise.reject({ status, message, data });
    });
