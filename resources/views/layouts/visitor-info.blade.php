@extends('layouts.landing')

@section('content')
<section id="desc" name="desc">
    <div id="landing-form">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 qr-panel">

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Новый Клиент!</h3>
                        </div>
                        <div class="panel-body">
                            Вам в кабинет добавлены данные клиента: <b>{{ $entity->name }}</b> <b>{{ $entity->surname }}</b>
                        </div>
                    </div>


                    <div class="clearfix"></div>
                    <a href="/" class="btn btn-warning btn-lg"><< Назад</a>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection