@extends('layouts.landing')

@section('content')
<section id="desc" name="desc">
    <div id="landing-form">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h3>Представьтесь ;)</h3>
                    <br>
                    <form role="form" action="/visitor/create" method="post" enctype="plain">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="email1">Телефон</label>
                            <input type="text" name="phone" class="form-control" id="phone" placeholder="+79160000000" required>
                        </div>
                        <div class="form-group">
                            <label for="name1">Имя</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="Имя" required>
                        </div>
                        <div class="form-group">
                            <label for="name1">Фамилия</label>
                            <input type="text" name="surname" class="form-control" id="surname" placeholder="Фамилия" required>
                        </div>
                        <div class="form-group">
                            <label for="email1">Адрес Email</label>
                            <input type="email" name="email" class="form-control" id="email" placeholder="client@mail.ru" required>
                        </div>

                        <div class="form-group">
                            <label>Примечания</label>
                            <textarea class="form-control" name="message" rows="3" required></textarea>
                        </div>

                        <div class="form-group">
                            <label>Интересующие темы</label>
                            <div class="form-group">
                                <input type="checkbox" />
                                <label class="landing-label">Робототехника</label>
                                <input type="checkbox" />
                                <label class="landing-label">Искуственный Интеллект</label>
                                <input type="checkbox" />
                                <label class="landing-label">Космос</label>
                                <input type="checkbox" />
                                <label class="landing-label">Фотография</label>
                                <br />
                                <input type="checkbox" />
                                <label class="landing-label">Криптовалюты</label>
                                <input type="checkbox" />
                                <label class="landing-label">Машинное обучение</label>
                                <input type="checkbox" />
                                <label class="landing-label">Возобновляемая электроэнергия</label>
                            </div>
                        </div>


                        <br>
                        <button type="submit" class="btn btn-large btn-success">Отправить данные</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection