@extends('layouts.landing')

@section('content')
<section id="desc" name="desc">
    <div id="landing-form">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 qr-panel">

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">QR код для клиента <b>{{ $entity->name }}</b> <b>{{ $entity->surname }}</b></h3>
                        </div>
                        <div class="panel-body">
                            <img src="data:{{ $content_type }};base64,{{ $qr_data }}" />
                        </div>
                    </div>


                    <div class="clearfix"></div>
                    <a href="/" class="btn btn-warning btn-lg"><< Назад</a>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection