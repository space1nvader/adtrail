@extends('adminlte::layouts.app')

@section('contentheader_title')
    <h1><i class="fa fa-user"></i> Редактировать данные аккаунта</h1>
@endsection

@section('main-content')

    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{ $user->paymentLogs->where('is_complete', 1)->count() }} <sup style="font-size: 20px">Операций</sup></h3>
                    <p>Операций со счетом</p>
                </div>
                <div class="icon">
                    <i class="fa fa-line-chart"></i>
                </div>
                <a href="{{ route('payment.history') }}" class="small-box-footer">Просмотр <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{ $user->balance / 100 }} <sup style="font-size: 20px">руб.</sup></h3>

                    <p>Ваш текущий баланс</p>
                </div>
                <div class="icon">
                    <i class="fa fa-rub"></i>
                </div>
                {{--<p class="small-box-footer">-</p>--}}
                <a href="{{ route('payment.form') }}" class="small-box-footer">Пополнить баланс <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-send"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text" style="white-space: normal;">Стоимость отправки 1 SMS</span>
                    <span class="info-box-number">{{ variable('sms_price') / 100 }} руб.</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-child"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text" style="white-space: normal;">Стоимость покупки 1 лида</span>
                    <span class="info-box-number">{{ variable('lead_price') / 100 }} руб.</span>
                </div>
            </div>
        </div>
    </div>

    <div class="box box-success">
        <div class="box-body">
            {!! Form::model($user, [
                'method' => 'PATCH',
                'route' => 'account.update',
            ]) !!}
            @include ('vendor.adminlte.account._form')
            {!! Form::close() !!}
        </div>
    </div>

@endsection
