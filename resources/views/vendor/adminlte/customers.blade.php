@extends('adminlte::layouts.app')

@section('contentheader_title')
    <h1><i class="fa fa-child" aria-hidden="true"></i>&nbsp Управление клиентами <?=($campaign) ? '[ Компания : <b>'.$campaign->name.'</b>]  от (' . $campaign->created_at->format('d-M-Y H:i') . ')' : '';?></h1>
@endsection

@section('main-content')

    <div class="box box-success">
        <div class="box-body">

            <div class="row">
                <div class="col-md-3">
                    <a href="{{ $minimal_url }}" class="btn btn-block <?=(!$minimal) ? 'btn-info': 'btn-warning'?>"><?=(!$minimal) ? '<i class="fa fa-minus-square"></i> Минимальный список': '<i class="fa fa-plus-square"></i> Подробный список'; ?></a>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <select class="form-control js-change-select" title="Показать">
                            <option value="{{ \Illuminate\Support\Facades\URL::route('customers.index', \Illuminate\Support\Facades\Request::except('filter')) }}" @empty(request('filter')) selected @endempty>Показать все</option>
                            <option value="{{ \Illuminate\Support\Facades\Request::fullUrlWithQuery(['filter' => ['is_visit' => 1]]) }}" @if((request('filter')['is_visit'] ?? '-1') == 1) selected @endif>Просмотренные</option>
                            <option value="{{ \Illuminate\Support\Facades\Request::fullUrlWithQuery(['filter' => ['is_visit' => 0]]) }}" @if((request('filter')['is_visit'] ?? '-1') == 0) selected @endif>Не просмотренные</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    @if($not_paid_customers->count())
                        <div class="form-group">
                            <select class="form-control js-mass-select" title="Выбрать">
                                <option value="" selected disabled>Выбрать</option>
                                <option value="only">Выбрать на странице</option>
                                <option value="all">Выбрать все</option>
                                <option value="not">Ни одного</option>
                            </select>
                        </div>
                    @endif
                </div>
                <div class="col-md-3">

                </div>
            </div>

            <form action="{{ url('purchase/phones') }}" method="POST" class="inline js-buy-phone-numbers">
                @csrf
                <input type="hidden" id="count_all_in_db" value="{{ $not_paid_customers->count() }}">
                <input type="hidden" id="count_selected" value="0">
                <input type="hidden" id="price_one_num" value="{{ config('services.global.lead_price') / 100 }}">
                <input type="hidden" id="campaign_id" value="{{ isset($campaign) ? $campaign->id : '' }}">

                <button class="btn btn-success" style="display: none;" type="submit"><i class="fa fa-credit-card"></i> Купить номера (<span id="nm-count">0</span>шт.=<span id="nm-sum">0</span>руб.)</button>
            </form>
            {{--
            <br/>
            <br/>
            @if(!$not_paid_customers->count())
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-2">
                            <select class="form-control js-mass-select">
                                <option value="" selected disabled>Выбрать</option>
                                <option value="only">Выбрать на странице</option>
                                <option value="all">Выбрать все</option>
                                <option value="not">Ни одного</option>
                            </select>
                        </div>
                    </div>
                </div>
            @endif
            --}}

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>
                            {{--<input type="checkbox" name="select_all" value="1">--}}
                        </th>
                        <th>@sortablelink('phone', 'Телефон')</th>
                            {{--<th>@sortablelink('name', 'Имя')</th>--}}
                            {{--<th>@sortablelink('email', 'Email')</th>--}}
                        @if(!$minimal)
                            <th>@sortablelink('is_visit', 'Просмотрено')</th>
                            <th>@sortablelink('visit_at', 'Дата просмотра')</th>
                            <th>@sortablelink('is_sent', 'Отправлено')</th>
                            <th>@sortablelink('send_at', 'Дата отправки')</th>
                            {{--<th>@sortablelink('created_at', 'Дата создания')</th>--}}
                            {{--<th></th>--}}
                        @endif
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($customers as $customer)
                        <tr {{ ($customer->is_visit == 1) ? 'class=success' : ''}}{{ ($customer->is_visit > 1) ? 'class=danger' : ''}}>
                                <td title="{{ $customer->id }}">@if($customer->is_visit && !$customer->is_payed)<input type="checkbox"  name="selected[]" value="{{ $customer->id }}">@endif</td>
                                <td>{!! $customer->getPhoneForTable() !!} ({{ $customer->hash}})</td>
                                {{--<td>{{ $customer->name }}</td>--}}
                                {{--<td>{{ $customer->email }}</td>--}}
                            @if(!$minimal)
                                <td>{!! $customer->is_visit ? '<i class="fa fa-eye"></i> &nbsp['.$customer->is_visit.']' : '<i class="fa fa-eye-slash"></i>' !!} </td>
                                <td>{{ !empty($customer->visit_at) ? $customer->visit_at->format('d-M-Y H:i') : '' }}</td>
                                <td>{!! $customer->is_sent ? '<i class="fa fa-bell"></i> &nbsp['.$customer->is_sent.']' : '<i class="fa fa-bell-slash"></i>' !!} </td>
                                <td>{{ !empty($customer->send_at) ? $customer->send_at->format('d-M-Y H:i') : '' }}</td>
{{--                                <td>{{ !empty($customer->created_at) ? $customer->created_at->format('d-M-Y H:i') : '' }}</td>--}}
                                {{--<td>--}}
                                    {{--<a href="/sms/{{ $customer->id }}" name="Просмотр"><button class="btn btn-info btn-xs"><i class="fa fa-send" aria-hidden="true"></i></button></a>--}}
    {{--                                <a href="{{ route('admin.customers.show', $customer) }}" name="Просмотр"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> Просмотр</button></a>--}}
    {{--                                <a href="{{ route('admin.customers.edit', $customer) }}" name="Редактировать"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Редактировать</button></a>--}}
                                    {{--{!! Form::open([--}}
                                        {{--'method'=>'DELETE',--}}
                                        {{--'url' => route('admin.customers.destroy', $customer),--}}
                                        {{--'style' => 'display:inline'--}}
                                    {{--]) !!}--}}
                                    {{--{!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Удалить', array(--}}
                                            {{--'type' => 'submit',--}}
                                            {{--'class' => 'btn btn-danger btn-xs',--}}
                                            {{--'name' => 'Удалить',--}}
                                            {{--'onclick'=>'return confirm("Подтверждаете удаление?")'--}}
                                    {{--)) !!}--}}
                                    {{--{!! Form::close() !!}--}}
                                {{--</td>--}}
                            @endif
                        </tr>

                    @endforeach

                    </tbody>
                </table>
                @include('vendor.adminlte.layouts.partials.pagination', ['pages' => $customers])
            </div>
        </div>
    </div>

@endsection

