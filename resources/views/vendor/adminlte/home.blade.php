@php
    $stat = (isset($stat)) ? $stat : [];
@endphp

@extends('adminlte::layouts.app')

@section('contentheader_title')
    <h1><i class="fa fa-dashboard"></i> Главная</h1>
@endsection


@section('main-content')

    <div class="container-fluid spark-screen">

        <h2>Проекты рассылок</h2>

        <div class="row">

            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="{{ route('projects.create') }}" name="Создать">
                    <div class="info-box bg-aqua">
                        <span class="info-box-icon">+</span>
                        <div class="info-box-content">
                            <span class="info-box-number">&nbsp;</span>
                            <span class="info-box-number">Добавить проект</span>
                        </div>
                    </div>
                </a>
            </div>

            @foreach($projects as $project)
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="/campaigns/?project_id={{ $project->id }}" name="Открыть">
                    <div class="info-box {{ session()->get('current_project', null) == $project->id ? 'bg-green' : '' }}">
                        <span class="info-box-icon">{{ $project->campaigns_count }}</span>

                        <div class="info-box-content">
                            {{--<span class="info-box-text">--}}

                            {{--</span>--}}
                            <span class="info-box-number">{{ $project->name }}</span>
                            <span class="progress-description">{!! $project->amo_sync ? 'amo: '.$project->amo_host : '-' !!}</span>

                        </div>
                    </div>
                </a>
            </div>
            @endforeach

        </div>

        {{--<h2>Создать новую кампанию</h2>--}}

        {{--<div class="row">--}}
            {{--<div class="col-md-8">--}}
                {{--@if($projects->count() && \Illuminate\Support\Facades\Session::get('current_project'))--}}
                    {{--{!! Form::open([--}}
                        {{--'method' => 'POST',--}}
                        {{--'route' => ['campaigns.store'],--}}
                        {{--'class' => '',--}}
                        {{--'files' => true--}}
                    {{--]) !!}--}}
                    {{--@include('vendor.adminlte.campaigns._form')--}}
                    {{--{!! Form::close() !!}--}}
                    {{--@else--}}
                    {{--<p class="text-danger"><strong>* Для создания компании, нужно сначала {{link_to_route('projects.create', 'создать')}} и / или выбрать текущий проект</strong></p>--}}
                {{--@endif--}}
            {{--</div>--}}
        {{--</div>--}}

        <h2>Как запустить рассылку</h2>

        <div class="row">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/8G54zIajwtY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
@endsection
