@extends('adminlte::layouts.app')

@section('contentheader_title')
    <h1>Ваши посетители</h1>
@endsection

@section('main-content')

    <div class="box box-success">
        <div class="box-body">

            <br/>
            <br/>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>@sortablelink('id', 'ID')</th>
                        <th>Имя</th>
                        <th>Фамилия</th>
                        <th>Телефон</th>
                        <th>Email</th>
                        <th>Сообщение</th>
                        <th>@sortablelink('created_at', 'Дата создания')</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($entities as $entity)
                        <tr>
                            <td>{{ $entity->id }}</td>
                            <td>{{ $entity->name }}</td>
                            <td>{{ $entity->surname }}</td>
                            <td>{{ $entity->phone }}</td>
                            <td>{{ $entity->email }}</td>
                            <td>{{ $entity->message }}</td>
                            <td>{{ !empty($entity->created_at) ? $entity->created_at->format('d-m-Y H:i') : '' }}</td>
                            {{--<td>--}}
                                {{--<a href="/sms/{{ $entity->id }}" name="Просмотр"><button class="btn btn-info btn-xs"><i class="fa fa-send" aria-hidden="true"></i></button></a>--}}
{{--                                <a href="{{ route('admin.customers.show', $entity) }}" name="Просмотр"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> Просмотр</button></a>--}}
{{--                                <a href="{{ route('admin.customers.edit', $entity) }}" name="Редактировать"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Редактировать</button></a>--}}
                                {{--{!! Form::open([--}}
                                    {{--'method'=>'DELETE',--}}
                                    {{--'url' => route('admin.customers.destroy', $entity),--}}
                                    {{--'style' => 'display:inline'--}}
                                {{--]) !!}--}}
                                {{--{!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Удалить', array(--}}
                                        {{--'type' => 'submit',--}}
                                        {{--'class' => 'btn btn-danger btn-xs',--}}
                                        {{--'name' => 'Удалить',--}}
                                        {{--'onclick'=>'return confirm("Подтверждаете удаление?")'--}}
                                {{--)) !!}--}}
                                {{--{!! Form::close() !!}--}}
                            {{--</td>--}}
                        </tr>
                    @endforeach

                    </tbody>
                </table>
                @include('vendor.adminlte.layouts.partials.pagination', ['pages' => $entities])
            </div>
        </div>
    </div>

@endsection
