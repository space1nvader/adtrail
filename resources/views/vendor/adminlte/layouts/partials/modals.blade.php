<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            {{--
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Default Modal</h4>
            </div>
            <div class="modal-body">
                <p>One fine body&hellip;</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
            --}}
        </div>
    </div>
</div>


<div class="modal fade" id="modal-set-limit-send-customer">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Подтверждения запуска рассылки</h4>
            </div>
            <div class="modal-body">
                <div class="modal-msg"></div>
                <div class="form-group">
                    <label>Количество клиентов для рассылки:</label>
                    <input type="number" name="limit" min="0" class="form-control" placeholder="0">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
                <a href="" class="btn btn-primary btn-ajax" data-dismiss="modal">Подтвердить рассылку</a>
            </div>
        </div>
    </div>
</div>