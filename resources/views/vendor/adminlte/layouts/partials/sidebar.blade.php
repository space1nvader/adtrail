@php
 use \Illuminate\Support\Facades\Request;
@endphp

<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p style="overflow: hidden;text-overflow: ellipsis;max-width: 160px;" data-toggle="tooltip" title="{{ Auth::user()->name }}">{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
                </div>
            </div>
        @endif

        <!-- search form (Optional) -->
        {{--<form action="#" method="get" class="sidebar-form">--}}
            {{--<div class="input-group">--}}
                {{--<input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>--}}
              {{--<span class="input-group-btn">--}}
                {{--<button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>--}}
              {{--</span>--}}
            {{--</div>--}}
        {{--</form>--}}
        <!-- /.search form -->

        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MENU</li>
            <!-- Optionally, you can add icons to the links -->
            <li {!! Request::is('home') ? 'class="active"' : '' !!}><a href="{{ route('home') }}"><i class='fa fa-dashboard'></i> <span>Главная</span></a></li>
            <li {!! Request::is('projects') ? 'class="active"' : '' !!}><a href="{{ route('projects.index') }}"><i class='fa fa-building'></i> <span>Проекты</span></a></li>
            <li {!! Request::is('campaigns') ? 'class="active"' : '' !!}><a href="{{ route('campaigns.index') }}"><i class='fa fa-rocket'></i> <span>Кампании</span></a></li>
            <li {!! Request::is('customers') ? 'class="active"' : '' !!}><a href="{{ route('customers.index') }}"><i class='fa fa-child'></i> <span>Клиенты</span></a></li>

            <li {!! Request::is('account') ? 'class="active"' : '' !!}><a href="{{ route('account.edit') }}"><i class='fa fa-user'></i> <span>Аккаунт</span></a></li>


            @if(auth()->user()->isAdmin())
            <li class="header">ADMINISTRATOR</li>
            <li {!! Request::is('variables') ? 'class="active"' : '' !!}><a href="{{ route('variable.forms') }}"><i class='fa fa-cogs'></i> <span>Глобальные настройки</span></a></li>
            <li {!! Request::is('refill-form') ? 'class="active"' : '' !!}><a href="{{ route('payment.refill-form') }}"><i class='fa fa-rub'></i> <span>Пополнение счета</span></a></li>
            @endif

{{--            <li {!! Request::is('visitors') ? 'class="active"' : '' !!}><a href="/visitors"><i class='fa fa-user-secret'></i> <span>Посетители</span></a></li>--}}

            {{--<li class="treeview">--}}
                {{--<a href="#"><i class='fa fa-link'></i> <span>{{ trans('adminlte_lang::message.multilevel') }}</span> <i class="fa fa-angle-left pull-right"></i></a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="#">{{ trans('adminlte_lang::message.linklevel2') }}</a></li>--}}
                    {{--<li><a href="#">{{ trans('adminlte_lang::message.linklevel2') }}</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
        </ul>
    </section>
</aside>
