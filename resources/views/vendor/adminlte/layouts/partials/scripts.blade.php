
<script src="{{ mix('/js/app.js') }}" type="text/javascript"></script>
<script src="{{ mix('/js/common.js') }}" type="text/javascript"></script>

<script>
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});
toastr.options = {
    "closeButton": true,
    "progressBar": true,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "6000",
    "extendedTimeOut": "1000",
}
</script>

@php
    $flashKeys = [
        'warning',
        'success',
        'info',
        'error',
    ]
@endphp
<script>

    //----------------------------- Пореход по ссылке через AJAX ---------------------------------//

    //------------- Открыть модалку для ввода количесва номеров для отправки --------------//
    $('.js-set-limit-send-customer').on('click', function (e) {
        e.preventDefault()
        var $modal = $('#modal-set-limit-send-customer')
        $this= $(this)

        $('.modal-body .modal-msg').html($this.data('confirm'))
        $('.modal-body input[name="limit"]').val($this.data('max-limit')).attr('max', $this.data('max-limit'))
        $modal.find('.btn-ajax').data('url', $this.data('url'))
        $modal.modal('show')
    })

    $(document).on('click', '.btn-ajax', function (e) {
        e.preventDefault()
        let $this = $(this),
            url = $this.data('url'),
            confirmMsg = $this.data('confirm'),
            method = $this.data('method') || 'GET',
            modal = $this.data('modal') || '#modal-default',
            reloadPage = $this.data('reload'),
            redirectTo = $this.data('redirect-to'),
            limit = $this.closest('.modal-content').find('input[name="limit"]').val()

        if (confirmMsg) {
            if (! confirm(confirmMsg)) {
                return;
            }
        }

        $.ajax({
            url: url,
            data: {'limit': limit},
            type: method,
            success: function (result) {

                let toastrType = result.toastr
                if (result.message) {
                    if (toastrType) {
                        toastr[toastrType](result.message)
                    } else {
                        alert(result.message)
                    }
                }

                if (result.modal_content) {
                    $(modal + ' .modal-content').html(result.modal_content)
                    $(modal).modal('show')
                }

                if (result.redirect || redirectTo) {
                    window.location.href = result.redirect || redirectTo;
                }

                if (result.reload || reloadPage) {
                    location.reload();
                }
            },
            error: function (error) {
                alert(error.statusText + ' ' + error.status)

                if (reloadPage) {
                    location.reload();
                }
            }
        })
    })

    @foreach ($flashKeys as $keyName)
        @if (\Illuminate\Support\Facades\Session::has($keyName))
        toastr.{{$keyName}}("{{ \Illuminate\Support\Facades\Session::get($keyName) }}");
        @endif
    @endforeach

    @if ($errors->any())
        @foreach ($errors->all() as $error)
        toastr.error('{{ $error }}');
        @endforeach
    @endif
</script>

@stack('scripts')
