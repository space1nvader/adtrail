@extends('adminlte::layouts.app')

@section('contentheader_title')
    <h1>Создать новый проект</h1>
@endsection

@section('main-content')
    <div class="box box-success">
        <div class="box-body">
            <a href="{{ route('projects.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Все проекты</button></a>
            <br />
            <br />
            {!! Form::open([
                'method' => 'POST',
                'route' => 'projects.store',
                'class' => '',
                'files' => true
            ]) !!}
            @include ('vendor.adminlte.projects._form')
            {!! Form::close() !!}
        </div>
    </div>
@endsection
