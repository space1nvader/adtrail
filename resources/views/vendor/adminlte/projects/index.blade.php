@extends('adminlte::layouts.app')

@section('contentheader_title')
    <h1><i class="fa fa-building"></i> &nbsp; Управление проектами</h1>
@endsection

@section('main-content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">
                <a href="{{ route('projects.create') }}" title="Create" class="btn btn-success"><i aria-hidden="true" class="fa fa-plus"></i> Создать Проект</a>
            </h3>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>@sortablelink('id', 'ID')</th>
                        <th>@sortablelink('name', 'Название')</th>
                        <th>@sortablelink('campaigns_count', 'Кол-во кампаний')</th>
                        <th>@sortablelink('amo_sync', 'Интеграция с АМО')</th>
                        <th>@sortablelink('created_at', 'Дата создания')</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($entities as $entity)
                        <tr>
                            <td>{{ $entity->id }}</td>
                            <td>
                                <a href="/projects/{{ $entity->id }}/edit" name="Редактировать">
                                    {{ $entity->name }} &nbsp; <i class="fa fa-edit" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td>
                                <a href="/campaigns/?project_id={{ $entity->id }}" name="Открыть">
                                    {{ $entity->campaigns_count }} &nbsp; <i class="fa fa-rocket" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td>{!! $entity->amo_sync ? '<i class="fa fa-check-square"></i> &nbsp['.$entity->amo_host.']' : '<i class="fa fa-square-o"></i>' !!}</td>
                            <td>{{ optional($entity->created_at)->format('d-m-Y H:i') }}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
                @include('vendor.adminlte.layouts.partials.pagination', ['pages' => $entities])
            </div>
        </div>
    </div>
@endsection
