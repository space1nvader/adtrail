@if ($message = \Illuminate\Support\Facades\Session::get('flash_error'))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
@endif

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Название Проекта', ['class' => 'control-label']) !!}
    <div class="">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@if (isset($contact_fields) && $contact_fields)
    <div class="form-group">
        <label>Поле для импорта контакта в AMO</label>
        <select name="amo_phone_field_id" title="Выберете поле для импорта" class="form-control">
            <option value="" disabled="disabled" selected="selected">Выберете поле для импорта</option>
            @foreach($contact_fields as $field)
                <option value="{{ array_get($field, 'id', 0) }}" {{ array_get($field, 'id', 0) == $entity->amo_phone_field_id ? 'selected="selected"' : '' }}>{{ array_get($field, 'name', 0) }}[{{ array_get($field, 'code', 0) }}]</option>
            @endforeach
        </select>
    </div>
@endif

<div class="form-group {{ $errors->has('amo_sync') ? 'has-error' : ''}}">
    <div class="checkbox">
        <label>
            {!! Form::hidden('amo_sync', 0, null) !!}
            {!! Form::checkbox('amo_sync', 1, null, ['class' => 'toggle-display-block', 'data-block' => '.amo-option']) !!} Интеграция с АМО-crm
        </label>
    </div>
</div>

<div class="amo-option">
    <div class="form-group {{ $errors->has('amo_host') ? 'has-error' : ''}}">
        {!! Form::label('amo_host', 'АМО-домен', ['class' => 'control-label']) !!}
        <div class="">
            {!! Form::text('amo_host', null, ['class' => 'form-control']) !!}
            {!! $errors->first('amo_host', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('amo_login') ? 'has-error' : ''}}">
        {!! Form::label('amo_login', 'АМО-логин', ['class' => 'control-label']) !!}
        <div class="">
            {!! Form::text('amo_login', null, ['class' => 'form-control']) !!}
            {!! $errors->first('amo_login', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('amo_api_key') ? 'has-error' : ''}}">
        {!! Form::label('amo_api_key', 'АМО-api-токен', ['class' => 'control-label']) !!}
        <div class="">
            {!! Form::text('amo_api_key', null, ['class' => 'form-control']) !!}
            {!! $errors->first('amo_api_key', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>

<div class="box-footer" style="padding: 10px 0;">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
</div>


@push('styles')
@endpush

@push('scripts')
@endpush