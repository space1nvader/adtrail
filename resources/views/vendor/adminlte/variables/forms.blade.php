@extends('adminlte::layouts.app')

@section('contentheader_title')
    <h1><i class="fa fa-cogs"></i> Глобальные настройки</h1>
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Цены</h3>
                </div>
                <form action="{{ route('variable.save') }}" method="POST">
                    <div class="box-body">
                        @csrf
                        <input type="hidden" name="group" value="prices">
                        <div class="form-group {{ $errors->has('vars.sms_price') ? 'has-error' : ''}}">
                            <label for="sms_price">Стоимость за 1 SMS, руб.</label>
                            <input type="number" min="0" step="0.001" class="form-control" id="sms_price" name="vars[sms_price]" value="{{ variable('sms_price') / 100 }}">
                            {!! $errors->first('vars.sms_price', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {{ $errors->has('vars.lead_price') ? 'has-error' : ''}}">
                            <label for="lead_price">Стоимость покупки лида, руб.</label>
                            <input type="number" min="0" step="0.001" class="form-control" id="lead_price" name="vars[lead_price]" value="{{ variable('lead_price') / 100 }}">
                            {!! $errors->first('vars.lead_price', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
