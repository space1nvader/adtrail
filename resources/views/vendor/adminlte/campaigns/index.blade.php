@extends('adminlte::layouts.app')

@section('contentheader_title')
    <h1><i class="fa fa-rocket"></i> &nbsp Управление кампаниями <?=($project) ? '[ Проект : <b>'.$project->name.'</b>]' : '';?></h1>
@endsection

@section('main-content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">
                <a href="{{ route('campaigns.create') }}" title="Create" class="btn btn-success"><i aria-hidden="true" class="fa fa-plus"></i> &nbsp Создать Новую Кампанию</a>
            </h3>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>@sortablelink('id', 'ID')</th>
                        <th>@sortablelink('project_name', 'Проект')</th>
                        <th>@sortablelink('name', 'Название')</th>
                        <th>@sortablelink('customers_count', 'Кол-во клиентов')</th>
                        <th width="100">@sortablelink('visits_count', 'Кол-во переходов')</th>
                        <th>Конверсия %</th>
                        <th>@sortablelink('message', 'Сообщение')</th>
                        <th>@sortablelink('link', 'Ссылка')</th>
                        <th style="width: 100px;">@sortablelink('created_at', 'Дата создания')</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($campaigns as $campaign)
                        @php
                            $visits_count = $campaign->customers->where('is_visit', '>', 0)->count('is_visit');
                        @endphp
                        <tr>
                            <td>{{ $campaign->id }}</td>
                            <td>{{ optional($campaign->project)->name }}</td>
                            <td>
                                <a href="/campaigns/{{ $campaign->id }}/edit" name="Редактировать">
                                    {{ $campaign->name }} &nbsp <i class="fa fa-edit" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td width="150"><b>{{ $campaign->customers_count }}</b> [ <a href="/customers/?campaign_id={{$campaign->id}}"><i class="fa fa-child" aria-hidden="true"></i> клиенты</a> ]</td>
                            <td width="50">{{ $campaign->customers->where('is_visit', '>', 0)->count('is_visit') }}</td>
                            <td width="100">{!!  $campaign->getConversion($visits_count) !!}</td>
                            <td>{{ mb_strimwidth($campaign->message, 0, 40, "...") }}</td>
                            <td>{{ mb_strimwidth($campaign->link, 0, 40, "...") }}</td>
                            <td>{{ !empty($campaign->created_at) ? $campaign->created_at->format('d-m-Y H:i') : '' }}</td>
                            <td  style="width: 40px;" id="list-campaigns-{{ $campaign->id }}">
                                @if($campaign->mailing_status == \App\Models\Campaign::MAILING_STATUS_FRESH)
                                <button class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="left" title="Отправить sms клиентам({{ $campaign->customers_count  }})"
                                        @click="startConfirm($event, {{$campaign->id}}, {{$campaign->customers_count}}, '{{$campaign->message}}')">
                                    <i class="fa fa-send" aria-hidden="true"></i>
                                </button>
                                @elseif($campaign->mailing_status == \App\Models\Campaign::MAILING_STATUS_SUCCESS)
                                <button class="btn btn-success btn-xs disabled" data-toggle="tooltip" data-placement="left" title="Рассылка успешно завершена">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </button>
                                @elseif($campaign->mailing_status == \App\Models\Campaign::MAILING_STATUS_RUNNING)
                                <button class="btn btn-warning btn-xs disabled" data-toggle="tooltip" data-placement="left" title="Рассылка уже запущена">
                                    <i class="fa fa-hourglass-half" aria-hidden="true"></i>
                                </button>
                                @elseif($campaign->mailing_status == \App\Models\Campaign::MAILING_STATUS_SUSPENDED_BALANCE || $campaign->mailing_status == \App\Models\Campaign::MAILING_STATUS_COMPLETED_PARTIALLY)
                                <a href="{{ route('campaign.sms', $campaign) }}" data-url="{{ route('campaign.sms', $campaign) }}" class="js-set-limit-send-customer" data-max-limit="{{ $campaign->customers_not_sent_count }}" data-confirm ='Уверены что хотите отправить sms клиентам({{ $campaign->customers_not_sent_count }}) этой кампании? Текст сообщения: {{ $campaign->message }}'>
                                    <button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="left" title="Возобновить рассылку sms клиентам({{ $campaign->customers_not_sent_count  }})" >
                                        <i class="fa fa-send" aria-hidden="true"></i>
                                    </button>
                                </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
                @include('vendor.adminlte.layouts.partials.pagination', ['pages' => $campaigns])
            </div>

        </div>

        <div class="overlay" v-cloak v-show="ajaxLoading">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>

    <v-dialog/>
@endsection

