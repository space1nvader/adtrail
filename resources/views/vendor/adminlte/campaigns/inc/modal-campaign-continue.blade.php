<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Запуск рассылки</h4>
</div>
<div class="modal-body">
    {!! $content !!}
</div>
<div class="modal-footer">
    @if(in_array('close', $buttons))
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
    @endif
    @if(in_array('refill', $buttons))
    <a href="{{ route('payment.form') }}" class="btn btn-success">Пополнить баланс</a>
    @endif
    @if(in_array('start', $buttons))
    <a href="{{ route('campaign.sms', [$campaign, 'force_start' => 1]) }}" class="btn btn-primary">Подтвердить рассылку</a>
    @endif
</div>