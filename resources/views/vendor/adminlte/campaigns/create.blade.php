@extends('adminlte::layouts.app')

@section('contentheader_title')
    <h1>Создать кампанию</h1>
@endsection

@section('main-content')
    <div class="box box-success">
        <div class="box-body">
            <a href="{{ route('campaigns.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Назад</button></a>
            <br />
            <br />
            {{--<div class="alert alert-info alert-dismissible">--}}
                {{--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>--}}
                {{--<h4><i class="icon fa fa-info"></i> Внимание!</h4>--}}
                {{--На данный момент вы можете разослать только 1000 сообщений в сутки по всем кампаниям--}}
                {{--<br />--}}
                {{--Совсем скоро будет доступна неограниченная рассылка--}}
            {{--</div>--}}

            {!! Form::open([
                'method' => 'POST',
                'route' => ['campaigns.store'],
                'class' => 'campaign-form',
                'files' => true
            ]) !!}
            @include ('vendor.adminlte.campaigns._form')
            {!! Form::close() !!}
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@endsection
