@extends('adminlte::layouts.app')

@section('contentheader_title')
    <h1>Редактировать "{{ $campaign->name }}"</h1>
@endsection

@section('main-content')

    @include('vendor.adminlte.layouts.partials.notifications')

    <div class="box box-success">
        <div class="box-body">
            <a href="{{ route('campaigns.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Назад</button></a>
            <br />
            <br />
            {!! Form::model($campaign, [
                'method' => 'PATCH',
                'route' => ['campaigns.update', $campaign->id],
                'class' => 'campaign-form',
                'files' => true
            ]) !!}
            @include ('vendor.adminlte.campaigns._form')
            {!! Form::close() !!}
        </div>
    </div>
@endsection
