<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Ссылка на кампанию + файл с данными</h3>
    </div>

    <div class="box-body">
        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('name', 'Название Кампании', ['class' => 'control-label']) !!}
            <div class="">
                {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Megafon']) !!}
                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('link') ? 'has-error' : ''}}">
            {!! Form::label('link', 'Ссылка на рекламную комппанию', ['class' => 'control-label']) !!}
            <div class="">
                {!! Form::text('link', null, ['class' => 'form-control']) !!}
                {!! $errors->first('link', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('message') ? 'has-error' : ''}}">
            {!! Form::label('theme', 'Рекламное сообщение', ['class' => 'control-label']) !!}
            <span>( Вместо переменной {{ config('services.sms.link_template') }} будет подставлена ваша ссылка, не забудьте указать ее в тексте Рекламного Сообщения )</span>
            <div class="">
                {!! Form::textarea('message', null, ['class' => 'form-control', 'id' => 'campaign_message_sms_text', 'placeholder' => '- 30% от цены, осталось 3 дня. Ваш GetTrail >> '.config('services.sms.link_template'), 'data-link-template' => config('services.sms.link_template')]) !!}
                {!! $errors->first('message', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="form-group"><span id="campaign_message_char_num">Осталось 134</span></div>
        </div>

        @empty($campaign)
            <div class="form-group">
                <label>Csv файл с даными</label>
                <input type="file" class="form-control" name="csv" id="csv" required>
            </div>
        @endif

        <div class="form-group {{ $errors->has('notify_email') ? 'has-error' : ''}}">
            {!! Form::label('notify_email', 'Email для нотификаций о переходах', ['class' => 'control-label']) !!}
            <div class="">
                {!! Form::text('notify_email', null, ['class' => 'form-control']) !!}
                {!! $errors->first('notify_email', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('notify_pack_count') ? 'has-error' : ''}}">
            {!! Form::label('notify_pack_count', 'Кол-во переходов для отправки email', ['class' => 'control-label']) !!}
            <div class="">
                {!! Form::number('notify_pack_count', $campaign->notify_pack_count ?? 10, ['class' => 'form-control', 'min' => 1]) !!}
                {!! $errors->first('notify_pack_count', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        @if(empty($campaign))
        <div class="form-group">
            <label>Проект</label>
            <select name="project_id" title="Ваш текущий проект" class="form-control" required>
                <option value="" disabled="disabled" selected="selected">Укажите текущий проект</option>
                @foreach($projects as $project)
                    <option value="{{ $project->id }}" {{ ($project->id == session()->get('current_project'))  ? 'selected="selected"' : '' }}> {{ $project->name }}</option>
                @endforeach
            </select>
        </div>
        @else
        <div class="form-group">
            <label>Проект</label>
            <select name="project_id" title="Ваш текущий проект" class="form-control" required>
                <option value="" disabled="disabled" selected="selected">Укажите текущий проект</option>
                @foreach($projects as $project)
                    <option value="{{ $project->id }}" {{ ($project->id == session()->get('current_project') || $project->id === $campaign->project_id)  ? 'selected="selected"' : '' }}> {{ $project->name }}</option>
                @endforeach
            </select>
        </div>
        @endif



        <div class="box-footer" style="padding: 10px 0;">
            {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
        </div>

    </div>
</div>


@push('styles')
@endpush

@push('scripts')
    <script>
        $('.campaign-form').on('submit', function (e) {
            e.preventDefault()

            var $form = $(this)
                $msgTextarea = $form.find('#campaign_message_sms_text'),
                string = $msgTextarea.val(),
                expr = $msgTextarea.data('link-template')

            if (! string.match(expr)) {
                if (! confirm('В вашем рекламном сообшении нет или неправильно указана рекламная ссылка, уверены что хотите продолжить?')) {
                    return
                }
            }
            this.submit()
        })
    </script>
@endpush