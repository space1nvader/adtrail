@extends('adminlte::layouts.app')

@section('contentheader_title')
    <h1><i class="fa fa-rub"></i> Пополнение счета пользователей</h1>
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-6">
            <admin-refill></admin-refill>
        </div>
    </div>
    <v-dialog/>
@endsection
