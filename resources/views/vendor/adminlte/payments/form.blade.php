@extends('adminlte::layouts.app')

@section('contentheader_title')
    <h1>Пополнение счета</h1>
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('payment.request') }}" method="POST">
                @csrf
                <div class="box box-success">
                    <div class="box-body">
                        {{--<div class="input-group">--}}
                            {{--<span class="input-group-addon"><i class="fa fa-rub"></i></span>--}}
                            {{--<input type="number" name="sum" class="form-control" max="100000" min="1" placeholder="200" required>--}}
                            {{--<span class="input-group-addon">.00</span>--}}
                        {{--</div>--}}
                        <div class="callout callout-warning">
                            <h4>Внимание</h4>

                            <p>На данный момент пополнение средств возможно только в ручном режиме,
                                сообщите о своих намерениях в форме ниже и мы свяжемся с вами</p>

                        </div>
                        <p>Ваш email: <?=auth()->user()->email?></p>
                        <div class="form-group">
                            <label>Телефон для связи</label>
                            <input type="text" name="phone" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Текс запроса</label>
                            <textarea class="form-control" name="message" rows="3" placeholder="Хочу пополнить счет на 2000руб"></textarea>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right">Отправить запрос</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection
