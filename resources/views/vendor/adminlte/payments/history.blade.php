@extends('adminlte::layouts.app')

@section('contentheader_title')
    <h1>История операций со счетом</h1>
@endsection

@section('main-content')

    <div class="box box-success">
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped text-center">
                    <thead>
                    <tr>
                        <th>@sortablelink('id', 'ID')</th>
                        <th>Описание</th>
                        <th>@sortablelink('created_at', 'Дата')</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($paymentLogs as $entity)
                        <tr>
                            <td style="width: 30px;">{{ $entity->id }}</td>
                            <td>{{ $entity->description }}</td>
                            <td style="width: 100px;">{{ optional($entity->created_at)->format('d-m-Y H:i:s') }}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
                @include('vendor.adminlte.layouts.partials.pagination', ['pages' => $paymentLogs])
            </div>
        </div>
    </div>
@endsection
