@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    Payment Result
@endsection

@section('content')
    <body class="hold-transition login-page">
    <div>
        <div class="login-box">
            <div class="login-logo">
                <a href="{{ url('/home') }}"><b>Get</b>Trail</a>
            </div><!-- /.login-logo -->

            {!! $msg ?? '' !!}


        </div>
    </div>
    @include('adminlte::layouts.partials.scripts_auth')
    </body>

@endsection
