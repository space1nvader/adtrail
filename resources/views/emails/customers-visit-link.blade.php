@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            {{ config('app.name') }}
        @endcomponent
    @endslot

    {{-- Body --}}
    У вас на компании <b>"{{ $campaign->name }}"</b> по ссылке {!! $campaign->link !!} новые переходы!
    @component('mail::table')
        |  Телефон       | Дата         | Кол-во посещений(всего) |
        | :------------: |:------------:| :----------------------:|
        @foreach($customers as $customer)
                |{!! substr_replace($customer->phone,'X XXX XX-XX', 3, 8) !!}|{!! $customer->visit_at->format('d-m-Y H:i') !!}|{!! $customer->is_visit !!}
        @endforeach
    @endcomponent

    @slot('footer')
        @component('mail::footer')
            &copy; {{ date('Y') }} {{ config('app.name') }}
        @endcomponent
    @endslot

@endcomponent