@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            {{ config('app.name') }}
        @endcomponent
    @endslot

    {{-- Body --}}

    #У вас новый запрос на оплату от пользователя

    Аккаунт
    * id: {{ $user->id }}
    * email: {{ $user->email }}

    Телефон: {{ $phone }}
    Сообщение: {{ $message }}


    @slot('footer')
        @component('mail::footer')
            &copy; {{ date('Y') }} {{ config('app.name') }}
        @endcomponent
    @endslot

@endcomponent