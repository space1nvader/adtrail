@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Gettrail - Error
@endsection

@section('contentheader_title')
    Gettrail - Error
@endsection

@section('main-content')

    <div class="error-page">
        <h2 class="headline text-red">500</h2>
        <div class="error-content">
            <h3><i class="fa fa-warning text-red"></i> {{ $exception->getMessage() }}.</h3>
            <p>
                {{ trans('adminlte_lang::message.notfindpage') }}
                {{--                {{ trans('adminlte_lang::message.mainwhile') }} <a href='{{ url('/home') }}'>{{ trans('adminlte_lang::message.returndashboard') }}</a> {{ trans('adminlte_lang::message.usingsearch') }}--}}
            </p>
            {{--<form class='search-form'>--}}
            {{--<div class='input-group'>--}}
            {{--<input type="text" name="search" class='form-control' placeholder="{{ trans('adminlte_lang::message.search') }}"/>--}}
            {{--<div class="input-group-btn">--}}
            {{--<button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i></button>--}}
            {{--</div>--}}
            {{--</div><!-- /.input-group -->--}}
            {{--</form>--}}
        </div><!-- /.error-content -->
    </div><!-- /.error-page -->
@endsection