<?php

use Illuminate\Database\Seeder;

class ContentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = factory(\App\User::class, 10)->create();
        $users->each(function ($user) {

            $projects = factory(\App\Project::class, rand(1, 2))->create(['user_id' => $user]);
            $projects->each(function ($project) {
                factory(\App\Campaign::class, rand(1,3))->create(['project_id' => $project->id]);
            });
        });
    }
}
