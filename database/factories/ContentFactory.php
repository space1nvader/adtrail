<?php

use Faker\Generator as Faker;

$factory->define(App\Project::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'amo_sync' => rand(0, 1),
        'amo_host' => 'dom'.$faker->randomDigit.'.amocrm.com',
        'amo_login' => $faker->email,
        'amo_api_key' => str_random(10),
    ];
});

$factory->define(App\Campaign::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'notify_email' => $faker->email,
        'message' => $faker->sentence(5),
        'link' => $faker->url,
        'visits_count' => $faker->randomDigit,
    ];
});
