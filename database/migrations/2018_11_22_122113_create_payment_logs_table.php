<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_logs', function (Blueprint $table) {
            $table->increments('id');

            $table->string('cause'); // причина: robokassa, sms,...

            $table->integer('sum')->default(0); // сумма операции, коп.
            $table->unsignedInteger('count')->default(0); // количество, например отправленных СМС

            $table->unsignedInteger('user_id');

            $table->tinyInteger('status')->default(0); // 0-Ожидает оплаты,1-Успешно оплачено,2-Проводится оплата,3-Ошибка оплаты
            $table->tinyInteger('is_complete')->default(0); // 0-Запись не завершена,1-Запись завершена
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_logs');
    }
}
