<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignTable extends Migration
{

    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('link');
            $table->text('message');
            $table->integer('is_sent')->default(0);
            $table->timestamps();
        });
    
        Schema::table('customers', function (Blueprint $table) {
            $table->integer('campaign_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::drop('campaigns');
        
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('campaign_id');
        });
    }
}
