<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAmoFieldsInProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->boolean('amo_sync')->default(0);

            $table->string('amo_host')->nullable()->change();
            $table->string('amo_login')->nullable()->change();
            $table->string('amo_api_key')->nullable()->change();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropSoftDeletes();

            $table->dropColumn('amo_sync');

            $table->string('amo_host')->nullable(false)->change();
            $table->string('amo_login')->nullable(false)->change();
            $table->string('amo_api_key')->nullable(false)->change();
        });
    }
}
