<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeNotifyNotRequired extends Migration
{
    public function up()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->integer('notify_pack_count')->default(null)->nullable()->change();
            $table->string('notify_email')->default(null)->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->integer('notify_pack_count')->default(10)->change();
            $table->string('notify_email')->change();
        });
    }
}
