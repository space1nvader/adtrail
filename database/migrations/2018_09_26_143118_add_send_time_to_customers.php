<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSendTimeToCustomers extends Migration
{

    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dateTime('send_at')->nullable()->after('is_sent');
        });
    
        Schema::table('campaigns', function (Blueprint $table) {
            $table->integer('notify_pack_count')->default(10)->after('is_sent'); // сколько за раз будет уходить лидов на канал
            $table->string('notify_email')->after('is_sent'); // супер быстрый способ sms?(за деньги) email? telegram
            $table->integer('visits_count')->default(0)->after('is_sent');
        });
    }


    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('send_at');
        });
    
        Schema::table('campaigns', function (Blueprint $table) {
            $table->dropColumn('notify_email');
            $table->dropColumn('notify_pack_count');
            $table->dropColumn('visits_count');
        });
    }
}
