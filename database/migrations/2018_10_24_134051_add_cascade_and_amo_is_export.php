<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCascadeAndAmoIsExport extends Migration
{
    
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->integer('campaign_id', false, true)->change();
            $table->boolean('amo_is_exported')->default(0);
            
            $table->foreign('campaign_id')->references('id')->on('campaigns')->onDelete('CASCADE');
        });
    }

    
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropForeign('customers_campaign_id_foreign');
            
            $table->dropColumn('amo_is_exported');
        });
    }
}
