<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCrmFieldsAndProjects extends Migration
{
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->boolean('js_check')->default(0);
            $table->dateTime('js_check_at')->nullable();
        });
        
        Schema::table('campaigns', function (Blueprint $table) {
            $table->boolean('amo_sync')->default(0);
        });
    
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('name');
            $table->string('amo_host');
            $table->string('amo_login');
            $table->string('amo_api_key');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('js_check');
            $table->dropColumn('js_check_at');
        });
        
        Schema::table('campaigns', function (Blueprint $table) {
            $table->dropColumn('amo_sync');
        });
    
        Schema::dropIfExists('projects');
    }
}
