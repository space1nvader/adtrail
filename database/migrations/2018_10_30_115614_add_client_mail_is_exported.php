<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClientMailIsExported extends Migration
{
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->boolean('mail_is_exported')->default(0);
        });
    }
    
    
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('mail_is_exported');
        });
    }
}
