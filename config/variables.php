<?php

/*
|--------------------------------------------------------------------------
|  Laravel Variables
|--------------------------------------------------------------------------
|
*/
return [

    'model' => \Fomvasss\Variable\Variable::class,

    'table_name' => 'variables',

    /* -----------------------------------------------------------------
     |  Root key for config, example: 'vars'
     |  Use: config('vars.some_var')
     |  If empty this - option OFF
     | -----------------------------------------------------------------
     */
    'config_key_for_vars' => 'vars',

    /* -----------------------------------------------------------------
     |  Replace configs with variables
     | -----------------------------------------------------------------
     */
    'variable_config' => [
        'app_name' => 'app.name',                   // config('app.name')
        'sms_price' => 'services.sms.price',        // config('services.sms.price')
        'lead_price' => 'services.global.lead_price',     // config('services.global.lead_price')
    ],

    /* -----------------------------------------------------------------
     |  Cache settings for vars
     | -----------------------------------------------------------------
     */
    'cache' => [

        'time' => 360,

        'name' => 'laravel.variables.cache',
    ]
];
