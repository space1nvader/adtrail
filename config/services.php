<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    
    'p1sms' => [
        'url' => env('P1_SEND_URL'),
        'login' => env('P1_LOGIN'),
        'pass' => env('P1_PASS'),
        'from' => env('P1_FROM'),
        'key' => env('P1_APIKEY'),
    ],
    
    'sms' => [
        'mode' => env('SMS_MODE', 'DEV'),
        'price' => env('SMS_PRICE', 250), // коп.
        'link_template' => env('SMS_LINK_TEMPLATE', '%LINK%'),
        'customer_price_to_buy' => env('CUSTOMER_PRICE_TO_BUE', 50), // price to buy viewing numbers, коп.
    ],

    'robokassa' => [
        'merch_login' => env('ROBOKASSA_MERCH_LOGIN', ''),
        'pass1' => env('ROBOKASSA_PASS1', ''),
        'pass2' => env('ROBOKASSA_PASS2', ''),
        'testmode' => env('ROBOKASSA_TESTMODE', true),
        'pass3' => env('ROBOKASSA_PASS3', ''),
        'pass4' => env('ROBOKASSA_PASS4', ''),
        'debug' => env('ROBOKASSA_DEBUG', true),
    ],
    
    'telegram' => [
        'chat_id' => env('TELEGRAM_CHAT_ID'),
        'bot_token' => env('TELEGRAM_BOT_TOKEN'),
        'proxy' => env('TELEGRAM_PROXY', null),
    ],
    
    'amo' => [
        'import' => env('AMO_IMPORT', false),
        'host' => env('AMO_HOST'),
        'login' => env('AMO_LOGIN'),
        'api_key' => env('AMO_API_KEY')
    ],
    
    'global' => [
        'lead_price' => env('GLOBAL_LEAD_PRICE', 500),
    ]

];
