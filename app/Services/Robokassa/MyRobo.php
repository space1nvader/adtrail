<?php
/**
 * Created by PhpStorm.
 * User: its
 * Date: 21.11.18
 * Time: 16:27
 */

namespace App\Services\Robokassa;

class MyRobo
{
    /** @var \App\Services\Robokassa  */
    protected $robo;

    protected $config;

    /**
     * PaymentController constructor.
     */
    public function __construct()
    {
        $this->config = config('services.robokassa');
    }

    public function make()
    {
        $this->robo = new Robokassa([
            $this->config['merch_login'],
            [
                $this->config['pass1'],
                $this->config['pass2'],
                $this->config['pass3'],
                $this->config['pass4'],
            ],
            $this->config['testmode'],
            $this->config['debug'],
        ]);

        return $this->robo;
    }
}