<?php

namespace App\Listeners;

use App\Events\CustomerDoVisit;
use Carbon\Carbon;

class IncrementCounters
{
    /**
     * Handle the event.
     *
     * @param  CustomerDoVisit  $event
     * @return void
     */
    public function handle(CustomerDoVisit $event)
    {
        if($event->customer->send_at !== null && $event->customer->send_at->diffInSeconds(Carbon::now()) > 10) {

            if ($event->customer->visit_at == null) {
                $event->customer->visit_at = Carbon::now();
            }
            $event->customer->increment('is_visit', 1);
    
            $event->customer->campaign->increment('visits_count', 1);
    
            $event->customer->save();
        }
    }
}
