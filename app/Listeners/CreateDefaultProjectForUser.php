<?php

namespace App\Listeners;

use App\Events\UserCreated;
use Illuminate\Queue\InteractsWithQueue;

class CreateDefaultProjectForUser
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
        if ($user = $event->user) {
            $project = $user->projects()->create(['name' => 'Новый проект']);

            \Illuminate\Support\Facades\Session::put('current_project', $project->id);
        }
    }
}
