<?php

namespace App\Listeners;

use App\Models\Customer;
use App\Events\CustomerDoVisit;
use App\Mail\CustomersVisitLink;
use Illuminate\Support\Facades\Mail;

class CheckNotification
{
    /**
     * Handle the event.
     *
     * @param  CustomerDoVisit  $event
     * @return void
     */
    public function handle(CustomerDoVisit $event)
    {
        $campaign = $event->customer->campaign;
        
        if($campaign->notify_email && ($campaign->visits_count >= $campaign->notify_pack_count)) {
            
            // get all visiting customers
            $customers = Customer::where('campaign_id', $campaign->id)
                ->where('is_sent', '!=', 0)
                ->where('is_visit', '>', 0)
                ->where('mail_is_exported', '=', 0)
                ->orderBy('visit_at', 'desc')
                ->limit($campaign->notify_pack_count)
                ->get();
            
            if(count($customers) > 0) {
                
                try {
                    // send email for pack count visits customers
                    Mail::to($campaign->notify_email)
                        ->send(new CustomersVisitLink($campaign, $customers));
        
                    $to_export = $customers->pluck('id')->toArray();
                    Customer::whereIn('id', $to_export)->update(['mail_is_exported' => true]);
        
                } catch (\Exception $e) {
                    error_log($e->getTraceAsString());
                }
    
                // set count to 0
                $campaign->visits_count = 0;
                $campaign->save();
            }
        
        }
    }
}
