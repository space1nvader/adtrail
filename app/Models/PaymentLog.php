<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class PaymentLog extends Model
{
    use Sortable;

    /** @var int Новый платеж, ожидает оплаты */
    const STATUS_PAYMENT_WAITING = 0;

    /** @var int Оплата успешна */
    const STATUS_PAYMENT_SUCCESS = 1;

    /** @var int Ошибка оплаты */
    const STATUS_PAYMENT_FAIL = 3;

    /** @var int Запись лога не завершена */
    const IS_COMPLETE_FALSE = 0;

    /** @var int Запись лога завершена */
    const IS_COMPLETE_TRUE = 1;

    protected $guarded = ['id'];

    public $sortable = [
        'id',
        'created_at',
        'updated_at',
   ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getDescriptionAttribute()
    {
        $sum = abs($this->sum / 100);
        $createdAt = optional($this->created_at)->format('d.m.Y h:i:s');

        if ($this->sum > 0) {
            $msg = "Пополнение счета ($this->cause) на $sum руб., $createdAt";
        } else {
            $msg = "Списание со счета $sum руб. за $this->count ($this->cause), $createdAt";
        }

        return $msg;
    }

    public function setComplete()
    {
        $this->is_complete = self::IS_COMPLETE_TRUE;
        $this->status = self::STATUS_PAYMENT_SUCCESS;

        return $this->save();
    }
}
