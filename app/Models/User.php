<?php

namespace App\Models;

use App\Events\UserCreated;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dispatchesEvents = [
        'created' => UserCreated::class,
    ];

    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function paymentLogs()
    {
        return $this->hasMany(PaymentLog::class);
    }

    public function isAdmin()
    {
        return $this->id == config('auth.admin_id');
    }
    
    /**
     * Receive and compare in kopeks
     * @param $cost
     * @return bool
     */
    public function isBalanceEnough($cost)
    {
        return $this->balance >= $cost;
    }
    
    public function decrementBlance($cost)
    {
        if($this->decrement('balance', $cost)) {
            PaymentLog::create([
                'cause' => 'visit',
                'user_id' => $this->id,
                'sum' => -1 * $cost,
                'count' => 1
            ])->setComplete();
            
            return true;
        }
        
        return false;
    }
}
