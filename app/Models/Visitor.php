<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Visitor extends Model
{
    use Sortable;
    
    protected $fillable = ['name', 'surname', 'email', 'phone', 'message'];
    
    public function visible()
    {
        return $this->where('visible', true);
    }
}
