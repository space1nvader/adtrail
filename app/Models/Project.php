<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Project extends Model
{
    use Sortable;

    protected $guarded = ['id'];

    public $sortable = [
        'id',
        'name',
        'amo_sync',
        'created_at',
        'updated_at',
    ];

    public $sortableAs = ['campaigns_count'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function campaigns()
    {
        return $this->hasMany(Campaign::class);
    }

    public function scopeByUser($query, $user_id)
    {
        return $query->where('user_id', $user_id);
    }
    
    public function checkAmoSync()
    {
        if($this->amo_sync) {
            try {
                // try to connect in amo
                $amo = new \AmoCRM\Client($this->amo_host, $this->amo_login, $this->amo_api_key);
                $amo->account->apiCurrent();
                
                if (! $this->amo_phone_field_id) {
                    return 'Поле для импорта контакта в AMO не настроено, его надо выбрать в <strong><a href="'.route('projects.edit', $this).'">настройках проекта.</a></strong>';
                }
            } catch (\Exception $e) {
                return 'Неверные данные для подключения к AmoCRM, укажите их в <strong><a href="'.route('projects.edit', $this).'">настройках проекта.</a></strong>';
            }
        }
    
        return false;
    }
}
