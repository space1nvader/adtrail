<?php

namespace App\Models;

use App\Jobs\ImportCrmLead;
use App\Jobs\SendSms;
use Illuminate\Database\Eloquent\Model;
//use Jelovac\Bitly4laravel\Facades\Bitly4laravel;
use Kyslik\ColumnSortable\Sortable;

class Customer extends Model
{
    use Sortable;
    
    public $sortable = ['id',
        'is_visit',
        'is_sent',
        'name',
        'email',
        'created_at',
        'updated_at',
        'visit_at',
        'amo_is_exported',
        'mail_is_exported'
    ];
    
    public $fillable = ['phone', 'email', 'hash', 'theme', 'send_at', 'amo_is_exported', 'mail_is_exported'];
    
    protected $dates = ['created_at', 'updated_at', 'visit_at', 'send_at'];
    
    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    public function scopeByUser($query, $user_id)
    {
        return $query->whereHas('campaign', function ($campaign) use ($user_id) {
            $campaign->byUser($user_id);
        });
    }

    public function scopeAllowedForPaid($query)
    {
        return $query->where('is_payed', 0)->where('is_visit', '<>', 0);
    }


    public function scopeFilterable($query, array $filters = [])
    {
        return $query->when(isset($filters['is_visit']), function ($q) use ($filters) {
            $q->where('is_visit', $filters['is_visit']);
        });
    }
    
    public function getLink()
    {
        return file_get_contents("https://2go.su/api/v2/action/shorten/?url=".config('app.url').'/visit/'.$this->hash);
//        return file_get_contents("https://clck.ru/--?url=".config('app.url').'/visit/'.$this->hash);

//        $bitly = Bitly4laravel::shorten(config('app.url').'/visit/'.$this->hash);
//
//        if($bitly and $bitly->status_code == 200) {
//            return $bitly->data->url;
//        }
//
//        return '';
    }
    
    public function getPhoneForTable() {
        if($this->is_payed) {
            return '<i class="fa fa-eye" aria-hidden="true"></i> '.$this->phone;
        }
        
        if($this->is_sent && $this->is_visit) {
            return '<i class="fa fa-cart-plus" aria-hidden="true"></i>
                <a @click="onPhoneClick($event, ' . $this->id . ')" class="purchase" href="#" title="Купить">'
                . substr_replace($this->phone,'X XXX XX-XX', 3, 8)
                . '</a>';
        }
        
        return substr_replace($this->phone,'X XXX XX-XX', 3, 8);
    }
    
    public function sendSms(array $params = [])
    {
        try {
            $config = config('services.p1sms');

            SendSms::dispatch($config, $this->id, $params)
                ->onQueue('sms');

            return true;
        } catch (\Exception $e) {
            error_log($e->getMessage());
            return false;
        }
    }
    
    public function importAmoLead($project)
    {
        try {
            $amo = new \AmoCRM\Client($project->amo_host, $project->amo_login, $project->amo_api_key);
            
            $contact = $amo->contact;
            $contact['name'] = $this->campaign->name;
            $contact['tags'] = ['gettrail'];
    
            $contact->addCustomField($project->amo_phone_field_id, [
                [$this->phone, 'WORK'],
            ]);

            $contact_id = $contact->apiAdd();
            // TODO: записывать $contact_id в контакт ?? для отслеживания
    
            $note = $amo->note;
            $note['element_id'] = $contact_id;
            $note['element_type'] = \AmoCRM\Models\Note::TYPE_CONTACT;
            $note['note_type'] = \AmoCRM\Models\Note::COMMON;
            $note['text'] = 'Gettrail. Переход по ссылке: ';
            $note['text'] .= str_replace(config('services.sms.link_template'), config('app.url').'/visit/'.$this->hash, $this->campaign->message);
    
            $note->apiAdd();
            
            $this->amo_is_exported = true;
            $this->save();
        } catch (\Exception $e) {
            error_log('Error (%d): %s', $e->getCode(), $e->getMessage());
        }
        
    }
    
    public function isNotExported()
    {
        return ($this->where('id', $this->id)->where('amo_is_exported', 0)->first()) ? true : false;
    }
    
    public function dispatchCrmExport()
    {
        $project = isset($this->campaign->project) ? $this->campaign->project : null;
        
        if($project && $project->amo_sync == 1 && $this->isNotExported()) {
            return dispatch(new ImportCrmLead($this->id))
                ->onQueue('crm');
        }
        
        return null;
    }
    
}
