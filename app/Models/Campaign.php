<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Kyslik\ColumnSortable\Sortable;

class Campaign extends Model
{
    use Sortable;
    
    const RETURN_TYPE_CRITICAL = 'critical';
    const RETURN_TYPE_ERROR = 'error';
    const RETURN_TYPE_REFILL = 'refill';
    const RETURN_TYPE_WARNING = 'refill';
    const RETURN_TYPE_SUCCESS = 'success';

    /** @var int Новая рассылка */
    const MAILING_STATUS_FRESH = 0;

    /** @var int Рассылка успешно завершена */
    const MAILING_STATUS_SUCCESS = 1;

    /** @var int Рассылка запущена/работает */
    const MAILING_STATUS_RUNNING = 2;

    /** @var int Рассылка преостановлена - нет денег */
    const MAILING_STATUS_SUSPENDED_BALANCE = 3;

    /** @var int Рассылка завершена частично */
    const MAILING_STATUS_COMPLETED_PARTIALLY = 4;

    public $sortable = [
        'id',
        'is_visit',
        'name',
        'email',
        'created_at',
        'updated_at',
        'visit_at'
    ];
    
    protected $fillable = [
        'name',
        'link',
        'message',
        'notify_email',
        'notify_pack_count',
        'project_id'
    ];

    public $sortableAs = ['customers_count'];
    
    protected $dates = ['created_at', 'updated_at'];
    
    public function customers()
    {
        return $this->hasMany(Customer::class);
    }

    public function customersNotSent()
    {
        return $this->hasMany(Customer::class)->where('is_sent', 0);
    }
    
    public function getLink()
    {
        $link = $this->link;
        
        $scheme = parse_url($this->link, PHP_URL_SCHEME);
        
        if (empty($scheme)) {
            $link = 'http://' . ltrim($this->link, '/');
        }
        
        return $link;
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function scopeByUser($query, $user_id)
    {
        return $query->whereHas('project.user', function ($user) use ($user_id) {
            $user->where('id', $user_id);
        });
    }
    
    public static function addNewWithCsvImport($request) {
        // Start transaction
        DB::beginTransaction();
        
        $campaign = new self();
        $campaign->fill($request->all());
        $campaign->save();
        
        $stat = [];

        $stat['campaign'] = $campaign;

        if($request->file('csv')) {
            $tmp_path = $request->file('csv')->getRealPath();
        
            $row = 0;
            $row_field_phone = 0;

            if (($handle = fopen($tmp_path, "r")) !== false) {
                while (($data = fgetcsv($handle, 1000, ",")) !== false) {

                    if (preg_match("/^7\d{10}/", $data[0])) {
                        $customer_data = [
                            'phone' => $data[0],
                            'hash' => md5(implode(';', $data) . date('Ymdhis')),
//                        'email' => '-',
//                        'theme' => '-',
                        ];

                        try {
                            $campaign->customers()->create($customer_data);
                            $campaign->save();
                        } catch (\Exception $e) {
                            DB::rollBack();
                            abort(500, $e->getMessage());
                        }
                    } else {
                        $row_field_phone++;
                    }

                    $row++;
                }
            
                fclose($handle);
                DB::commit();
            }
        
            $stat['rows_count'] = $row;
            $stat['rows_field_phone_count'] = $row_field_phone;
        }
        
        return $stat;
    }
    
    public function checksBeforeStart($customers_count)
    {
        if( $result = $this->failedProjectOrAmo()) {
            return $result;
        }
    
        if( $result = $this->failedCustomersCount($customers_count)) {
            return $result;
        }
    
        if( $result = $this->failedCustomerBalance($customers_count)) {
            return $result;
        }
    }
    
    public function getCustomers($customers_count)
    {
        return $this->customers()
            ->where('is_sent', 0)
            ->when($customers_count, function ($q) use ($customers_count) {
                $q->limit($customers_count);
            })->get();
    }
    
    private function failedProjectOrAmo()
    {
        if ($message = $this->project->checkAmoSync()) {
            return [
                'type' => self::RETURN_TYPE_ERROR,
                'message' => $message
            ];
        }
        
        return false;
    }
    
    private function failedCustomersCount($count)
    {
        // TODO : тогда эта компания должна обладать соответствующим статусом
        if (!$count) {
            return [
                'type' => self::RETURN_TYPE_ERROR,
                'message' => 'ВНИМАНИЕ! В кампании нет клиентов, которым не отправлено рассылки!'
            ];
        }
        
        return false;
    }
    
    private function failedCustomerBalance($count)
    {
        $total_price = $count * config('services.sms.price');
        $user_balance = $this->project->user->balance;
        
        if ($user_balance < config('services.sms.price')) {
            return [
                'type' => self::RETURN_TYPE_REFILL,
                'message' => 'ВНИМАНИЕ! На вашем счету <b>недостаточно средств для начала кампании</b>. Ваш текущий баланс = '
                    . ($user_balance / 100) .' руб. Нужно ' . ($total_price / 100)
                    . ' руб. Необходимо <a href="/payment"><b>пополнить Ваш баланс </b></a>'
            ];
        }
        
        return false;
    }
    
    public function fireSmsSending($customers)
    {
        $i = $e = 0;
    
        $paymentLog = PaymentLog::create([
            'cause' => 'sms',
            'user_id' => $this->project->user->id,
        ]);
    
        $customers_ids = $customers->pluck('id')->toArray();
    
        foreach ($customers as $customer) {
            $queued = $customer->sendSms([
                'payment_log_id' => $paymentLog->id,
                'queue_customers' => $customers_ids,
            ]);
            
            ($queued) ? $i++ : $e++;
        }
    
        $type = self::RETURN_TYPE_SUCCESS;
        $message = 'Успешно <b>отправлены в очередь</b> на отправку <b> ('.$i.')</b> sms. '
            .'Узнать точный статус можно в подробной информации по клиентам кампании'
            .'<a href="/customers/?campaign_id='.$this->id.'"> <b>'.$this->name.' </b></a>';
    
        if($e != 0) {
            $type = self::RETURN_TYPE_WARNING;
            $message = '<b>ВНИМАНИЕ!</b> Не доставлено ('.$e.') и Успешно отправлены ('.$i.') sms';
        }
        
        return [ 'type' => $type, 'message' => $message ];
    }
    
    public function getConversion($visits_count)
    {
        if( $visits_count == 0) {
            return '<small class="label label-table bg-gray">0 %</small>';
        }
        if ($this->mailing_status == self::MAILING_STATUS_SUCCESS && $visits_count !== 0) {
            $percent = round($visits_count / ($this->customers_count / 100), 2);
            
            if($percent < 10) {
                return '<small class="label label-table bg-orange">'.$percent.' %</small>';
            } if ($percent < 20) {
                return '<small class="label bg-aqua-active">'.$percent.' %</small>';
            }
            
            return '<small class="label label-table bg-green">'.$percent.' %</small>';
        }
        
        return '-';
    }
}
