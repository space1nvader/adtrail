<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class PaymentRequest extends Mailable
{
    use Queueable, SerializesModels;
    
    public $user;
    public $phone;
    public $message;
    
    public function __construct($user, $phone, $message)
    {
        $this->user = $user;
        $this->phone = $phone;
        $this->message = $message;
    }
    
    public function build()
    {
        return $this->from($this->user->email)
            ->markdown('emails.payment-request');
    }

}