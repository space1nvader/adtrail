<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomersVisitLink extends Mailable
{
    use Queueable, SerializesModels;

    public $campaign;
    public $customers;
    
    public function __construct($campaign, $customers)
    {
        $this->campaign = $campaign;
        $this->customers = $customers;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(['address' => 'no-reply@gettrail.me', 'name' => 'Gettrail.ru'])
            ->subject('Новые лиды от Gettrail.ru')
            ->markdown('emails.customers-visit-link');
    }
}
