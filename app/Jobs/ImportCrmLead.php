<?php

namespace App\Jobs;

use App\Models\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportCrmLead implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    public $customer_id;

    public function __construct($customer_id)
    {
        $this->customer_id = $customer_id;
    }
    
    public function handle()
    {
        try {
            $customer = Customer::find($this->customer_id);
    
            $campaign = $customer->campaign;
            $project = $campaign->project;
    
            $amo = new \AmoCRM\Client($project->amo_host, $project->amo_login, $project->amo_api_key);
        
            $contact = $amo->contact;
            $contact['name'] = $campaign->name;
            $contact['tags'] = ['gettrail'];
        
            $contact->addCustomField($project->amo_phone_field_id, [
                [$customer->phone, 'WORK'],
            ]);
        
            $contact_id = $contact->apiAdd();
            // TODO: записывать $contact_id в контакт ?? для отслеживания
        
            $note = $amo->note;
            $note['element_id'] = $contact_id;
            $note['element_type'] = \AmoCRM\Models\Note::TYPE_CONTACT;
            $note['note_type'] = \AmoCRM\Models\Note::COMMON;
            $note['text'] = 'Gettrail. Переход по ссылке: ';
            $note['text'] .= str_replace(config('services.sms.link_template'), config('app.url').'/visit/'.$customer->hash, $campaign->message);
        
            $note->apiAdd();
        
            $customer->amo_is_exported = true;
            $customer->save();
        } catch (\Exception $e) {
            error_log("job[ImportCRMLead]Error ({$e->getCode()}): {$e->getMessage()}");
        }
    }
}
