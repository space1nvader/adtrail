<?php

namespace App\Jobs;

use App\Models\Campaign;
use App\Models\Customer;
use App\Models\PaymentLog;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendSms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    public $config;

    public $customer_id;

    public $payment_log_id;

    public $queue_customers;
    
    
    /**
     * SendSms constructor.
     * @param $config
     * @param $phone
     * @param $message
     */
    public function __construct($config, $customer_id, array $params = [])
    {
        $this->config = $config;
        $this->customer_id = $customer_id;
        $this->payment_log_id = $params['payment_log_id'] ?? null;
        $this->queue_customers = $params['queue_customers'] ?? null;

    }
    
    public function handle()
    {
        $result = null;

        if ($customer = Customer::with('campaign.customers')->find($this->customer_id)) {

            $campaign = $customer->campaign;

            $user = $campaign->project->user;

            $smsPrice = config('services.sms.price');

            // Рассылка приостановлена из-за нехватки средств!
            if (($user->balance - $smsPrice) < 0) {
                $campaign->mailing_status = Campaign::MAILING_STATUS_SUSPENDED_BALANCE;
                $campaign->save();

                $this->paymentLogSetComplete();
                return null;
            }

            $link = $customer->getLink();

            if ($link != '') {

                $message = str_replace(config('services.sms.link_template'), $link, $customer->campaign->message);

                if (config('services.sms.mode') == 'PROD') {
                    $this->sendSms($customer, $message);
                } elseif (config('services.telegram.chat_id')) {
                    $this->sendTelegram($message);
                } else {
                    \Log::info($customer->phone .': '. $message);
                }

                $customer->is_sent = true;
                $customer->send_at = Carbon::now();

                $result = $customer->save();

                // Снимаем деньги с баланса юзера
                $user->decrement('balance', $smsPrice);

                // Добавляем снятие денег (минусуем) к запии в истории
                // Добавляем количество отправленных
                if ($paymentLog = PaymentLog::find($this->payment_log_id)) {
                    $paymentLog->decrement('sum', $smsPrice);
                    $paymentLog->increment('count');
                }

                // Рассылка запущена/работает!
                if ($campaign->mailing_status != Campaign::MAILING_STATUS_RUNNING) {
                    $campaign->mailing_status = Campaign::MAILING_STATUS_RUNNING;
                    $campaign->save();
                }

                // Рассылка успешно завершена!
                $campaignCustomers = $campaign->customers()->get();
                if ($campaignCustomers->where('is_sent', 1)->count() == $campaignCustomers->count()) {
                    $campaign->mailing_status = Campaign::MAILING_STATUS_SUCCESS;
                    $campaign->save();

                    $this->paymentLogSetComplete();
                }
                // Частична рассылка успешно завершена!
                elseif ($this->queue_customers && ! $campaign->customers()->where('is_sent', 0)->whereIn('id', $this->queue_customers)->count()) {
                    $campaign->mailing_status = Campaign::MAILING_STATUS_COMPLETED_PARTIALLY;
                    $campaign->save();

                    $this->paymentLogSetComplete();
                }

            } else {
                \Log::error('Sending error. Message for customer has not link!');
                // Рассылка компании завершена не успешно! Нет ссылки для отправки
                $campaign->mailing_status = Campaign::MAILING_STATUS_SUCCESS;
                $campaign->save();

                $this->paymentLogSetComplete();
            }
        }

        
        return $result;
    }
    
    private function sendSms($customer, $message)
    {
        $client = new Client();
    
        return $client->get(
            $this->config['url'] . '?user=' . $this->config['login'] . '&pwd=' . $this->config['pass']
            . '&sadr=' . $this->config['from'] . '&dadr=' . $customer->phone . '&text=' . $message
        );
    }
    
    private function sendTelegram($message)
    {
        $proxy = (config('services.telegram.proxy')) ? ['proxy' => config('services.telegram.proxy')] : [];
        $client = new Client($proxy);
    
        $telegram_message = urlencode("adtrail devmode sms >> {$message}\n\n");
    
        return $client->get('https://api.telegram.org/bot' . config('services.telegram.bot_token')
            . '/sendMessage?chat_id=' . config('services.telegram.chat_id') . '&text=' . $telegram_message);
    }

    /**
     * Запись лога (журлана) платежей - завершена.
     * @return bool
     */
    private function paymentLogSetComplete()
    {
        if ($this->payment_log_id && ($paymentLog = PaymentLog::find($this->payment_log_id))) {
            return $paymentLog->setComplete();
        }

        return false;
    }
}
