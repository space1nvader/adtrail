<?php

namespace App\Events;

use App\Models\Customer;
use Illuminate\Queue\SerializesModels;

class CustomerDoVisit
{
    use SerializesModels;
    
    public $customer;
    
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }
}
