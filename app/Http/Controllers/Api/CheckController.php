<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CheckController extends Controller
{
    public function checkJs()
    {
        $str = 'var url = "http://adtrail.1nspace.com/pop?cmp_id="+cmp_id+"&cid="+cid+"';
        $str .= '&ref="+escape(window.location.href); var cid = new URLSearchParams(window.location.search).get(\'cid\');';
        return $str . ' document.write(\' <img style="display: none" src="\'+url+\'" />\');';
    }
    
    public function checkDb(Request $request)
    {
        $client = Client::find($request->input('cid'));
        $client->js_check = true;
        $client->js_check_at = Carbon::now();
        $client->save();
    }
}