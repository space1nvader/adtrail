<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Campaign;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function purchaseCustomer(Request $request)
    {
        $customer_id = $request->input('customer_id');
        $customer = Customer::find($customer_id);
        
        if($customer && ! $customer->is_payed) {
            $cost = (int) config('services.global.lead_price');
    
            // проверить хватает ли бабок, если нет выплюнуть заявление что бабок нет
            if (! auth()->user()->isBalanceEnough($cost)) {
                return [
                    'result' => 'error',
                    'message' => 'Недостаточно средств на счету, пополните пожалуйста счет'
                ];
            }
            // если все ок, снимаем бабки и показываем номер
            if (auth()->user()->decrementBlance($cost)) {
                $customer->is_payed = true;
                $customer->save();
    
                /* Export lead in CRM after purchase */
                $customer->dispatchCrmExport();

                return [
                    'result' => 'success',
                    'message' => 'Вы приобрели номер '.$customer->phone,
                    'phone' => $customer->phone,
                    'balance' => auth()->user()->balance / 100
                ];
            }
        }

        return ['result' => 'error', 'message' => 'Этот номер уже куплен'];
    }
    
    public function startCampaign($id, Request $request)
    {
        if(!$campaign = Campaign::byUser(Auth::id())->find($id)) {
            return [
                'type' => Campaign::RETURN_TYPE_CRITICAL,
                'message' => 'Кампания с таким ID не найдена или не привязана к проекту'
            ];
        }
        
        $customers = $campaign->getCustomers($request->input('customers_count'));
        
        // 1) check all limits
        
        if($failed_info = $campaign->checksBeforeStart($customers->count())) {
            return $failed_info;
        }
        
        // 2) create tasks for queue
    
        return $campaign->fireSmsSending($customers);
        
    }
    
    public function searchByEmail(Request $request)
    {
        $query = $request->input('q');
        
        $users = User::where('email', 'like', "{$query}%")->limit(10)->get();
        
        $formatted_users = [];
    
        foreach ($users as $user) {
            $formatted_users[] = ['name' => $user->name.' : '.$user->email.' - '.($user->balance/100).'₽', 'id' => $user->id];
        }
        
        return $formatted_users;
    }
}