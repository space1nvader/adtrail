<?php


namespace App\Http\Controllers;

use App\Events\CustomerDoVisit;
use App\Models\Customer;
use App\Models\Visitor;
use Illuminate\Http\Request;
use CodeItNow\BarcodeBundle\Utils\QrCode;


class VisitorController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth')->only('index');
    }
    
    public function index(Visitor $visitors)
    {
        $entities = $visitors->visible()->sortable()->paginate(40);

        return view('vendor.adminlte.visitors', compact('entities'));
    }
    
    public function create(Request $request)
    {
        $visitor = new Visitor();
        $visitor->fill($request->input());
        $visitor->hash = md5(json_encode($visitor->attributesToArray()));
        
        // TODO: sms создан qr-код
        $visitor->save();
    
        $qrCode = new QrCode();
        
        $qrCode->setText(request()->getSchemeAndHttpHost().'/visitor/check/'.$visitor->hash)
            ->setSize(300)
            ->setPadding(10)
            ->setErrorCorrection('high')
            ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
            ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
            ->setLabelFontSize(16)
            ->setImageType(QrCode::IMAGE_TYPE_PNG);
        
        return view('layouts.qr', [
            'entity' => $visitor,
            'content_type' => $qrCode->getContentType(),
            'qr_data' => $qrCode->generate()
        ]);
    }
    
    public function check($md5)
    {
        $entity = Visitor::where('hash', $md5)->first();
        $entity->visible = true;
        $entity->save();
        
        if($entity) {
            return view('layouts.visitor-info', compact('entity'));
        }
        
        return view('404');
    }

    public function visit($hash)
    {
        $customer = Customer::where(['hash' => $hash])->first();

        if($customer->campaign) {

            event(new CustomerDoVisit($customer));
            return redirect()->away($customer->campaign->getLink());
        }

        return abort(404);
    }
}
