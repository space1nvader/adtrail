<?php

namespace App\Http\Controllers;

use App\Http\Requests\AccountRequest;
use App\Http\Requests\Admin\ProfileRequest;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{

    public function edit()
    {
        $user = \Auth::user();

        return view('vendor.adminlte.account.edit', compact('user'));
    }

    public function update(AccountRequest $request)
    {
        $request->password ? $request->merge(['password' => bcrypt($request->password)]) : $request->offsetUnset('password');

        $user = \Auth::user();

        $user->update($request->validated());

        return redirect()->back()
            ->with('success', trans('notifications.update.success'));
    }
}
