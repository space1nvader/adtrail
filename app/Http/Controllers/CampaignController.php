<?php

namespace App\Http\Controllers;

use App\Http\Requests\CampaignRequest;
use App\Models\Campaign;
use App\Models\Customer;
use App\Models\PaymentLog;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class CampaignController extends Controller
{
    /**
     * @param \App\Models\Campaign $campaigns
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $project = null;
        
        if ($project_id = $request->project_id) {
            
            $project = Project::byUser(Auth::id())->find($project_id);
            
            if($project) {
                $campaigns = Campaign::byUser(Auth::id())->withCount('customers')->where('project_id', $project->id)->sortable(['id' => 'desc'])->paginate(40);
            } else {
                $campaigns = Campaign::byUser(Auth::id())->withCount('customers')->sortable()->paginate(40);
            }
            
        } else {
            
            $campaigns = Campaign::byUser(Auth::id())
                ->withCount('customers')->withCount('customersNotSent')
                ->sortable()->orderBy('id', 'desc')->paginate(40);
        }
    
        $token = auth()->user()->createToken('gettrail')->accessToken;

        return view('vendor.adminlte.campaigns.index', compact('campaigns', 'project', 'token'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $projects = Project::byUser(Auth::id())
            ->withCount('campaigns')->sortable()->orderBy('id', 'desc')->get();

        return view('vendor.adminlte.campaigns.create', compact('projects'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CampaignRequest $request)
    {
        $stat = Campaign::addNewWithCsvImport($request);

        session()->flash('success', 'Успешно импортировано: ' . ($stat['rows_count'] - $stat['rows_field_phone_count']) . ' записей');
        if ($stat['rows_field_phone_count']) {
            session()->flash('warning', 'Не импортировано по причине не валидности тел. номера: ' . $stat['rows_field_phone_count'] . ' записей');
        }

        if (! empty($stat['campaign'])) {
            return redirect()->route('campaigns.edit', $stat['campaign']->id);
        }

        return redirect()->back();
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $campaign = Campaign::byUser(Auth::id())->findOrFail($id);
    
        $projects = Project::byUser(Auth::id())
            ->withCount('campaigns')->sortable()->orderBy('id', 'desc')->get();
        
        return view('vendor.adminlte.campaigns.edit', compact('campaign', 'projects'));
    }

    /**
     * @param $id
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, CampaignRequest $request)
    {
        $campaign = Campaign::byUser(Auth::id())->findOrFail($id);
        $campaign->update($request->all());
        
        return redirect()->route('campaigns.index')
            ->with('success', trans('notifications.update.success'));
    }

    /**
     * @param \App\Models\Customer $customers
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function customers(Request $request)
    {
        $campaign = null;

        $sort = ['visit_at' => 'desc',];
        $filter = $request->get('filter', []);

        if ($request->campaign_id && ($campaign = Campaign::byUser(Auth::id())->find($request->campaign_id))) {

            $customers = $campaign->customers()->sortable($sort)->filterable($filter)->paginate(40);
            $not_paid_customers = $campaign->customers()->allowedForPaid()->get();
        } else {

            $customers = Customer::byUser(Auth::id())->filterable($filter)->sortable($sort)->paginate(40);
            $not_paid_customers = Customer::byUser(Auth::id())->byUser(auth()->id())->allowedForPaid()->get();
        }
        

        $minimal = $request->input('minimal', 0);
        $minimal_url = $request->fullUrlWithQuery(['minimal' => !$minimal]);
    
        $token = auth()->user()->createToken('gettrail')->accessToken;

        return view('vendor.adminlte.customers', compact('customers', 'not_paid_customers', 'minimal', 'minimal_url', 'campaign', 'token'));
    }

    public function purchaseCustomers(Request $request)
    {
        $campaign_id = $request->campaign_id;
        $except = is_array($request->except) ? $request->except : [$request->except];

        $customersQ = Customer::byUser(auth()->id())
            ->when($campaign_id, function ($q) use ($campaign_id) {
                $q->where('campaign_id', $campaign_id);
            })
            ->when($request->except, function ($q) use ($except) {
                $q->whereNotIn('id', $except);
            })
            ->allowedForPaid();

        if ($request->type == 'all') {
            $customers = $customersQ->get();
        } else { //($request->type == 'only' && $request->only) {
            $only = is_array($request->only) ? $request->only : [$request->only];
            $customers = $customersQ
                ->whereIn('id', $only)
                ->get();
        }

        $customers_count = $customers->count();
        $cost = $customers_count * config('services.global.lead_price');

        if ($customers_count) {

            if (auth()->user()->isBalanceEnough($cost)) {
                auth()->user()->decrement('balance', $cost);

                $customers->each(function ($customer) {
                    $customer->setAttribute('is_payed', 1);
                    $customer->save();
                    
                    /* Export lead in CRM after purchase */
                    $customer->dispatchCrmExport();
                });

                PaymentLog::create([
                    'cause' => 'visit',
                    'user_id' => auth()->id(),
                    'sum' => -1*$cost,
                    'count' => $customers->count(),
                ])->setComplete();

                return response()->json([
                    'message' => "Успешно куплено ".$customers_count." номеров",
                    'toastr' => 'success',
                ]);
            } else {
                $msg = 'Не достаточно средств для покупки выбранных номеров!';
            }

        } else {
            $msg = 'Нет выбранных номеров для покупки!';
        }

        return response()->json([
            'message' => $msg,
            'toastr' => 'error',
        ]);
    }
    
    public function sms($customer_id, Request $request)
    {
        $customer = Customer::byUser(Auth::id())>findOrFail($customer_id);

        $paymentLog = PaymentLog::create([
            'cause' => 'sms',
            'user_id' => $customer->campaign->project->user->id,
        ]);

        $queued = $customer->sendSms(['payment_log_id' => $paymentLog->id]);

        if(!$queued) {
            $request->session()->flash('error', 'Проблемы на сервере, попробуйте позже');
        } else {
            $request->session()->flash('success', 'SMS успешно отправлено');
        }

        return redirect('customers');
    }
}