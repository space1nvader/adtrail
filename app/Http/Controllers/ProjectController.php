<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectRequest;
use App\Models\Project;
use Illuminate\Support\Facades\Session;

class ProjectController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entities = Project::where('user_id', auth()->id())
            ->withCount('campaigns')->sortable()->orderBy('id', 'desc')->paginate(40);

        return view('vendor.adminlte.projects.index', compact('entities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vendor.adminlte.projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectRequest $request)
    {
        if($request->amo_sync == true) {
            $amo = new \AmoCRM\Client($request->amo_host, $request->amo_login, $request->amo_api_key);

            try {
                $amo->account->apiCurrent();
            } catch (\Exception $e) {
                return redirect()->route('projects.create')
                ->with('error','Неверные данные для подключения к AmoCRM');

            }
        }

        $entity = Project::create($request->validated() + ['user_id' => auth()->id()]);

        return redirect()->route('projects.edit', ['id' => $entity->id])
            ->with('success','Проект '.$request->name.' успешно создан');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entity = Project::findOrFail($id);
    
        $contact_fields = null;
        
        if($entity->amo_sync) {
            $amo = new \AmoCRM\Client($entity->amo_host, $entity->amo_login, $entity->amo_api_key);
    
            try {
                $amo_data = $amo->account->apiCurrent();
                $contact_fields = array_get($amo_data, 'custom_fields.contacts', null);
            } catch (\Exception $e) {
                $entity->amo_sync = 0;
                $entity->save();

                return redirect()->route('projects.edit', ['id' => $entity->id])
                    ->with('error','Неверные данные для подключения к AmoCRM');
        
            }
        }
        
        return view('vendor.adminlte.projects.edit', compact('entity', 'contact_fields'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectRequest $request, $id)
    {
        $entity = Project::findOrFail($id);

        if ($request->amo_sync == true) {
            $amo = new \AmoCRM\Client($request->amo_host, $request->amo_login, $request->amo_api_key);

            try {
                $amo->account->apiCurrent();
            } catch (\Exception $e) {
                return back()
                    ->with('error','Неверные данные для подключения к AmoCRM');
            }
        }
        
        $entity->update($request->validated());

        return redirect()->route('projects.edit', ['id' => $entity->id])
            ->with('success', trans('notifications.update.success'));
    }

    public function currentSet($id)
    {
        $entity = Project::findOrFail($id);

        Session::put('current_project', $entity->id);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $entity = Project::findOrFail($id);
        $entity->delete();


        return redirect()->route('projects.index')
            ->with('success', trans('notifications.destroy.success'));
    }
}
