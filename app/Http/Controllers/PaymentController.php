<?php

namespace App\Http\Controllers;

use App\Mail\PaymentRequest;
use App\Models\PaymentLog;
use App\Models\User;
use App\Services\Robokassa\MyRobo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PaymentController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showForm()
    {
        return view('vendor.adminlte.payments.form');
    }
    
    public function request(Request $request)
    {
        $phone = $request->input('phone', null);
        $message = $request->input('message', null);
    
        # send email
        Mail::to('admin@gettrail.ru')
            ->send(new PaymentRequest(auth()->user(), $phone, $message));
        
        // TODO : Add telegram or sms integration notification
        
        return redirect()->back()
            ->with('success', "Заявка отправлена, скоро наш менеджер свяжется с Вами");
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Robokassa\MyRobo $robo
     */
    public function pay(Request $request, MyRobo $robo)
    {
//        $paymentLog = PaymentLog::create([
//            'cause' => 'robokassa',
//            'sum' => $request->sum * 100,
//            'user_id' => auth()->id(),
//        ]);
//
//        $robo->make()->doRedirect(
//            $request->sum, // outsum, rub.
//            $paymentLog->description, // payment desc
//            $paymentLog->id, // invoice_id
//            [
//                // you can get this into ResultURL $robo->get_shp_params();
//                'param'=>'helloworld', // without shp_
//                'email'=>'admin@app.com' ,
//	        ],
//	        'ru' // IncCurrLabel
//        );
    }

    public function refillForm()
    {
        $users = \App\Models\User::all();
    
        $token = auth()->user()->createToken('gettrail')->accessToken;
        return view('vendor.adminlte.payments.refill', compact('users', 'token'));
    }

    public function refill(Request $request)
    {
        $user = User::where('id', $request->input('user_id', null))->firstOrFail();

        $user->increment('balance', $request->input('sum') * 100);

        PaymentLog::create([
            'cause' => 'admin-refill',
            'sum' => $request->input('sum') * 100,
            'user_id' => $user->id,
        ])->setComplete();
    
        return [
            'type' => 'success',
            'message' => "Счет пользователя ({$user->id}) {$user->email} - успешно пополнен"
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showSuccess(Request $request)
    {
        \Log::info(__METHOD__, $request->all());

        return view('vendor.adminlte.payments.result-page', [
            'msg' => '<h2 class="text-center text-green">Поздравляем!<br>Платеж успешно проведен!</h2>',
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showFail(Request $request)
    {
        \Log::info(__METHOD__, $request->all());

        return view('vendor.adminlte.payments.result-page', [
            'msg' => '<h2 class="text-center text-red">Ошибка!<br>Платеж не проведен.</h2>',
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Robokassa\MyRobo $robo
     * @param $gatewayName
     * @return int
     */
    public function notify(Request $request, MyRobo $robo, $gatewayName)
    {
        \Log::info(__METHOD__, $request->all());

        if($gatewayName == 'robokassa' && $robo->make()->isSuccess()) {

            // we got shp_ params without shp_
            //$_REQUEST['InvId'];
            $shp_params = $robo->make()->get_shp_params();
            \Log::info(__METHOD__ . json_encode($shp_params));

            $paymentLog = PaymentLog::where('is_complete', 0)->findOrFail($request->InvId);
            $paymentLog->setComplete();

            if ($paymentLog->user) {
                $paymentLog->user->increment('balance', $paymentLog->sum);
            }

            return 1;
        }

        abort(404);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function history()
    {
        $paymentLogs = PaymentLog::where('user_id', auth()->id())
            ->where('is_complete', 1)->sortable(['created_at' => 'desc'])->paginate();

        return view('vendor.adminlte.payments.history', compact('paymentLogs'));
    }
}
