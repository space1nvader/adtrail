<?php

namespace App\Http\Controllers;

use Fomvasss\Variable\Models\Variable;
use Illuminate\Http\Request;

class VariableController extends Controller
{
    public function forms()
    {
        return view('vendor.adminlte.variables.forms');
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'vars' => 'array',
            'vars.*' => 'string',
        ]);

        foreach ($request->vars as $key => $value) {
            if ($request->group == 'prices') {
                $this->updateOrCreate($key, $value * 100);
            } else {
                $this->updateOrCreate($key, $value);
            }
        }

        \Cache::forget('laravel.variables.cache');
        \Artisan::call('config:clear');

        return redirect()->back()->with('success', trans('notifications.update.success'));
    }

    protected function updateOrCreate($key, $value)
    {
        Variable::updateOrCreate(['key' => $key], ['value' => $value]);
    }
}
