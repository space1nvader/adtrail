<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = \Auth::id();
        return [
            //'name' => 'sometimes|required|string|max:255',
            //'email' => 'sometimes|unique:users,email,'.$userId.'|email|required',
            //'password' => 'sometimes|nullable|alpha_num|between:6,20|confirmed',
            'password' => 'nullable|alpha_num|between:6,20|confirmed',
        ];
    }
}
















