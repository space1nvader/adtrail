<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:191',
            'amo_sync' => 'required|in:0,1',
            'amo_host' => 'required_if:amo_sync,1|nullable|string|max:191',
            'amo_login' => 'required_if:amo_sync,1|nullable|string|max:191',
            'amo_api_key' => 'required_if:amo_sync,1|nullable|string|max:191',
            'amo_phone_field_id' => 'nullable|integer',
        ];
    }
}
