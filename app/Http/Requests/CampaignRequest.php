<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CampaignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_id' => 'required|numeric',
            'name' => 'required|string',
            'link' => 'required|string',
            'message' => 'required|string|max:134',
            'notify_email' => 'email',
            'notify_pack_count' => 'numeric|min:3',
            'csv' => 'sometimes|file|mimes:csv,txt'
        ];
    }
}
