<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//Route::any('csv', 'HomeController@csv');
//Route::any('heelp', 'OuterController@heelp');


// Admin - resources
Route::group(['middleware' => 'auth'], function () {
    Route::get('home', 'HomeController@index')->name('home');

    Route::get('account', 'AccountController@edit')->name('account.edit');
    Route::patch('account', 'AccountController@update')->name('account.update');

    Route::resource('campaigns', 'CampaignController');
    Route::get('sms/{customer}', 'CampaignController@sms')->name('admin.customers.sms');
    
    Route::get('start-campaign/{campaign}', 'CampaignController@startCampaign')->name('start.campaign');
    
    Route::get('campaign-sms/{campaign}', 'CampaignController@campaignSms')->name('campaign.sms');
    Route::get('customers', 'CampaignController@customers')->name('customers.index');
    Route::post('purchase/phones', 'CampaignController@purchaseCustomers');

    Route::resource('projects', 'ProjectController');
    Route::post('projects/{id}/current', 'ProjectController@currentSet')->name('projects.current.set');

    Route::get('payment', 'PaymentController@showForm')->name('payment.form');
    Route::post('payment', 'PaymentController@pay')->name('payment.pay');
    Route::get('payment/history', 'PaymentController@history')->name('payment.history');
    Route::post('payment/request', 'PaymentController@request')->name('payment.request');

    Route::group(['middleware' => 'check.admin'], function () {
        Route::get('payment/refill-form', 'PaymentController@refillForm')->name('payment.refill-form');
        Route::post('payment/refill', 'PaymentController@refill')->name('payment.refill');
        Route::get('variables', 'VariableController@forms')->name('variable.forms');
        Route::post('variables', 'VariableController@save')->name('variable.save');
    });
});

Route::post('payment/{gatewayName}/notify', 'PaymentController@notify')->name('payment.notify');
Route::get('payment/success', 'PaymentController@showSuccess')->name('payment.success');
Route::get('payment/fail', 'PaymentController@showFail')->name('payment.fail');

Route::get('visitors', 'VisitorController@index');
Route::get('visit/{hash}', 'VisitorController@visit');
Route::post('visitor/create', 'VisitorController@create');
Route::get('visitor/check/{md5}', 'VisitorController@check');