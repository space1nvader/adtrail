<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth:api', 'check.admin']], function () {
    Route::get('/search-by-email', 'Api\UserController@searchByEmail');
    
    Route::post('payment/refill', 'PaymentController@refill')->name('payment.refill');
});

Route::group(['prefix' => 'user','middleware' => 'auth:api'], function () {
    Route::post('/purchase/phone', 'Api\UserController@purchaseCustomer');
    Route::post('/campaign/{id}/start', 'Api\UserController@startCampaign');
    
    Route::post('/payment/refill', 'PaymentController@refill')->name('payment.refill');
    
});

Route::group(['prefix' => 'v1','middleware' => 'auth:api'], function () {
    //    Route::resource('task', 'TasksController');

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_api_routes
});
