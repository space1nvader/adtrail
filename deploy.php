<?php
namespace Deployer;

require 'vendor/deployer/deployer/recipe/laravel.php';

set('application', 'gettrail');
set('repository', 'git@bitbucket.org:space1nvader/adtrail.git');

set('git_tty', true); // [Optional] Allocate tty for git on first deployment
add('shared_files', []);
add('shared_dirs', []);
add('writable_dirs', ['storage', 'node_modules']);
set('allow_anonymous_stats', false);
set('http_user', 'www-data');
set('writable_mode', 'chmod');
set('keep_releases', 3);
//set('ssh_multiplexing', false);

// Laravel writable dirs
set('writable_dirs', [
    'bootstrap/cache',
]);

set('bin/npm', function () {
    return run('which npm');
});

desc('npm i command');
task('npm:install', function () {
    if (has('previous_release')) {
        if (test('[ -d {{previous_release}}/node_modules ]')) {
            run('cp -R {{previous_release}}/node_modules {{release_path}}');
        }
    }
    run("cd {{release_path}} && {{bin/npm}} i");
});

desc('Run/Build npm release');
task('npm:run', function () {
    run("cd {{release_path}} && {{bin/npm}} run prod");
});

//desc('Passport deploy');
//task('passport:install', function () {
//    run("cd {{release_path}} && php artisan passport");
//});

// Hosts
host('adtrail.1nspace.com')
    ->user('www-data')
    ->stage('staging')
    ->set('deploy_path', '/var/www/adtrail');

// Hosts
host('89.223.93.123')
    ->user('www-data')
    ->stage('prod')
    ->set('deploy_path', '/var/www/adtrail');

/**
 * Main task
 */
desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
    'npm:install',
    'npm:run',
    'artisan:storage:link',
    'artisan:view:clear',
    'artisan:cache:clear',
    'artisan:config:cache',
    'artisan:optimize',
    'deploy:symlink',
    'deploy:unlock',
    'restart:supervisor',
    'cleanup',
]);

desc('Reload Supervisor');
task('restart:supervisor', function () {
    run('supervisorctl restart all');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');

